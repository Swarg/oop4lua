-- 20-11-2023 @author Swarg
--
-- Factory to provide new Interface
-- access (index) to table of this module generate a new interface
--
local base = require("oop.base")
local method_signature = require("oop.MethodSignature")
local dprint = base.dprint

--
--  Usage Example:
--
--  local EventListener = interface.EventListener:extends(Listener) {
--    on_event = interface.method('self', Event, 'event')
--  }
--  return interface.build(EventListener)
--

---class oop.Interface { __index: oop.Interface, _cname:string, _ctype:number }

---@class oop.interface_factory { __index: oop.interface_factory }

local M = {}
M.method = method_signature.new


-- base metatable for Interfaces
--
-- UseCase:
--  - bind hook on call table of interface for interface definition:
--    See M.on_call_interface()
--
--  - creating a new interface inherited from already existing:
--
--   local EventListener = class.interface.EventListener:extends(Listener) {
--      handle = ...
--   }

---@class oop.Interface
---@field _ctype number
---@field _cname string
---@field extends function(oop.Interface):oop.Interface
local Interface = { -- metatable(Class) for any new interface
  _ctype = base.INTERFACE,
  __tostring = base._tostring,
  getClassName = base.getClassName
}
--
Interface.__index = Interface

--
-- hook to set definition on call table of the Interface
-- provide ability to perform calls like:  MyInterface {..}
--
-- fires on try to call interface table as function
-- local Runnable = class.interface.Runnable({})        or Runnable { run = .. }
--                        ^step1   ^step2   ^step3
--
-- step 1 - switches to interface creation mode
-- step 2 - create a new interface with the given name "Runnable"
-- step 3 - calling the table of the newly created interface( this method)
--
---@generic T
---@param self `T` | oop.Interface
---@return T self
function Interface.__call(self, ...)
  dprint(1, 'Call Interface:', (self or {})._cname)

  -- allow to add methods only in definition stage
  assert(rawget(self, 'extends'), base.err_try_edit_interface)

  local arg_cnt = select('#', ...)
  local arg = arg_cnt > 0 and select(1, ...) or nil
  if arg_cnt == 1 and type(arg) == 'table' then
    base._copy_def(self, arg)
  else
    error('Expected one table with interface definition, has: ',
      arg_cnt .. ' ' .. type(arg))
  end

  return self
end

--
-- Check is current interface is an inheritor of the given Interface
--
-- self: oop.Interface
---klass: oop.Object|oop.Interface|table|string?
---return boolean
Interface.instanceof = base.instanceof -- Interface.instanceof(self, pa_iface)

--
-- interface inheritance:
-- available only in definition stage
--
-- local EventListener = interface.EventListener:extends(Listener) {..}
--
---@param parent oop.Interface
---@return self
local function extends(self, parent)
  dprint('Interface:extends', parent)
  assert(base.is_interface(parent), 'parent must be an interface')
  if base.is_interface(parent) then
    ---@diagnostic disable-next-line: inject-field
    setmetatable(self, parent)
  end
  return self
end

--
-- Create a new interface inherited given parent or default oop.Interface
--
---@param parent oop.Interface?
---@param name string?    the name of new interface
---@param def table?      definition of the interface: methods
---@return oop.Interface
function M.new(parent, name, def)
  assert(not parent or base.is_interface(parent), 'invalid interface parent')
  assert(not name or base.is_valid_classname(name),
    'invalid interface name: ' .. tostring(name))
  assert(not def or type(def) == 'table', 'invalid interface definition')

  parent = parent or Interface
  name = name or base.next_class_name(parent._cname, base.INTERFACE)
  name = base.ensure_with_package(name)

  local iface = def or {} -- new interface
  iface._ctype = base.INTERFACE
  iface._cname = name
  iface.__index = iface
  iface.__call = Interface.__call
  iface.__tostring = rawget(parent, '__tostring') or base._tostring

  -- available only definition stage:
  iface.extends = extends

  iface = setmetatable(iface, parent)
  base.register(iface)
  return iface
end

--
-- finalize the interface definition
--
-- UseCase:
--   - set methods name for method-signatures
--   - replace type Object for self to Actual Interface
--   (if self already not defined to specific Class)
--
---@generic T
---@param interface `T`|oop.Interface
---@return T
function M.build(interface)
  assert(base.is_interface(interface), 'interface expected')

  for imname, msign in pairs(interface) do
    --? idea: replace Object from self to Interface itself
    if base.is_method_signature(msign) then
      ---@cast msign oop.MethodSignature
      msign:bind(interface, imname)
    end
  end

  rawset(interface, 'extends', nil) -- available only in definition stage
  return interface
end

--  via interface('EventListener', Listener)
--
--
---@diagnostic disable-next-line: unused-vararg
local function create_new_interface(...)
  error('Not Implements yet')
end

local interface_mt = {
  -- interface(), interface('MyInterface'),  interface('MyInterface', IParent)
  __call = function(_, ...)
    return create_new_interface(...) -- in development
  end,

  -- to create a new Interface via access to non-exists filed of the table
  ---@return oop.Interface
  __index = function(self, key) -- class.MyClass
    dprint(1, 'interface: access to field:', key)
    self = self
    -- here key is interface name

    return M.new(nil, key, nil)
  end
}

return setmetatable(M, interface_mt) -- return M
