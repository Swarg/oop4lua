-- module oop.MethodSignature
--
--   This module provide "class" oop.MethodSignature whose task is hold a data
-- about method signatures of interfaces or abstract classes.
--   This data is filled in at the time of description of the interface or
-- abstract method and will be used on call class.build() (sealing the class)
--   This method causes a full validation of the declared class, checking
-- whether all declared interfaces are actually implemented.
--   And if there are unimplemented interface methods, then there is a detailed
-- display of what has not yet been implemented. It will be displayed in
-- detailed text-message which specific method with which signature in this
-- class has not yet been implemented.
--
-- Goal: used to define methods in the interfaces and abstract classes
-- on call - throw an error warnings

local base = require("oop.base")
local dprint = base.dprint


--- class oop.MethodSignature: { throws: fun(...):self, __call:fun(...), _ctype:number}

---@class oop.MethodSignature
---@field _cname string method name
---@field ['self'] table|nil
---@field ['return'] any
local M = {}
M.__index = M
M.__call = base.abstract_method
-- __tostring will be below

local v2s, fmt = tostring, string.format

--
-- Create a new method signature (function signature)
-- used to define interface or abstract class
-- Example:
--  handle = interface.method(EventListener, 'self', Event, 'event'),
--                            ^^ type of self                ^^ arg1-name
--                                                   ^^ type-of-arg1
--  ^^^^^^ defined method name
--
--  hasNext = interface.method('self', 'boolean')
--  ^ method         type-of-this ^      ^ return type
--                    oop.Object
--
-- new_method_signature
--
---@vararg oop.Object|oop.Interface|string|table
---@return oop.MethodSignature
function M.new(...)
  local o = { _ctype = base.METHOD_SIGNATURE }

  local i, cnt = 1, select('#', ...)
  while i <= cnt do
    -- type varname, type varname, ...
    local _type = select(i, ...)
    local varname = select(i + 1, ...)
    dprint(1, '_type:', _type, "varname:", (varname), 'i:', i, 'cnt:', cnt)


    -- for the first parameter "self"  do not need to specify the type
    if i == 1 and _type == 'self' then
      -- a way to get the class itself by its full name, independed of the module
      _type = base.class_for(base.ObjectClassName) or base.ObjectClassName
      varname = 'self'
      i = i - 1                              -- trick
    elseif cnt == 2 and _type == 'self' then -- case: self + return_type
      o['self'] = base.class_for(base.ObjectClassName) or base.ObjectClassName
      o['return'] = M.validated_type(_type, 'return')
      break
      -- for last param (the return type without varname)
    elseif i + 1 >= cnt and not varname then
      varname = 'return'
      -- to improve code readability
    elseif (_type == 'return' or _type == ':') and varname ~= nil then
      -- case: method(Object, 'self', type, 'arg1', 'return', type)
      -- case: method('self', ':', <return_type>)
      _type = varname
      varname = 'return'
    end

    if type(varname) ~= 'string' then
      error('varname must be a string, got: ' .. v2s(i) .. ': ' .. v2s(varname))
    end

    o[varname] = M.validated_type(_type, varname)
    i = i + 2
  end

  return setmetatable(o, M)
end

M.supported_extra_types = {
  -- void = 0, -- for specify no return value
  any = 1, -- to specify that method signature can consume or return any value
}

---@param _type any
---@param varname string
function M.validated_type(_type, varname)
  if varname == 'return' and _type == 'void' then
    return _type -- ok only as return type
  end
  if not base.is_type(_type) and not M.supported_extra_types[_type or 0] then
    error(fmt('expected simple type or custom Class, got: "%s" for vn:"%s"',
      v2s(_type), v2s(varname)))
  end
  return _type
end

--
--
---@param self table & oop.MethodSignature?
---@param fullclassname boolean? (def false)
-- function M.method_singature_tostring(t, fullclassname)
function M.__tostring(self, fullclassname)
  if base.is_method_signature(self) then
    local methodname = rawget(self, '_cname') or 'func'

    local s = base.type_tostring(self['return']) .. ' ' .. (methodname or '') .. '('
    local i = 0
    for varname, _type in pairs(self) do
      if not base.LIB_KEYS[varname] and varname ~= 'return' then
        if i > 0 then s = s .. ', ' end
        s = s .. base.type_tostring(_type, fullclassname) .. ' ' .. varname
        i = i + 1
      end
    end
    s = s .. ')'
    if type(self._throws) == 'table' and next(self._throws) then
      s = s .. ' throws '
      i = 0
      for _, e in pairs(self._throws) do
        if i > 0 then s = s .. ', ' end
        s = s .. base.type_tostring(e, false)
        i = i + 1
      end
      s = s .. ';'
    end
    return s
  end
  return tostring(self)
end

--
--
-- Replace oop.Object for self by specific interface
-- set method-name from given value or find from interface
-- Applyed on interface.build(Interface)
--
---@param interface oop.Interface
---@param method_name string?
function M:bind(interface, method_name)
  assert(base.is_interface(interface), 'interface')
  -- assert(base.is_method_signature(self), 'method_singature')

  if self.self and (self.self._cname == base.ObjectClassName) then
    self.self = interface
  end

  -- find the method name for self
  if not method_name then
    for mn, m in pairs(interface) do
      if m == self then
        method_name = mn
        break
      end
    end
  end

  rawset(self, '_cname', method_name) -- set method name

  return self
end

--
-- handle = interface.method(Type, 'varname'):throws(IOException)
-- MethodSignature.new(Type, 'varname'):throws(IOException)
--
---@return self
function M:throws(...)
  self._throws = self._throws or {}
  for i = 1, select('#', ...) do
    --  todo string or Class
    self._throws[#self._throws + 1] = select(i, ...)
  end
  return self
end

return M
