-- 17-11-2023 @author Swarg
local M = {}
--
--  Base Constants and shared inner logic
--


M.CLASS            = 1
M.ABSTRACT_CLASS   = 2
M.INTERFACE        = 8
M.ENUM             = 16
M.METHOD_SIGNATURE = 32


M.CTYPE = {
  [M.CLASS]            = 'Class',
  [M.ABSTRACT_CLASS]   = 'AbstractClass',
  [M.INTERFACE]        = 'Interface',
  [M.ENUM]             = 'Enum',
  [M.METHOD_SIGNATURE] = 'MethodSignature',
}

-- (in define|raw) unfinished class under definition

M.LIB_KEYS = {
  __index = true,
  __call = true,
  -- __tostring ?
  _cname = true,
  _ctype = true,
  _implements = true,
  _throws = true,
}

M.max_inherit_deep = 256

M.ObjectClassName = 'oop.Object'
M.InterfaceClassName = 'oop.Interface'

-- error notification
M.errmsg_only_for_classes = "this method can only be called for classes and not objects"
M.err_abstract_method_call = 'attempt to call an abstract method'

M.err_try_edit_interface = 'attempt to add methods to an interface after the completed definition'

M.abstract_method = function() error(M.err_abstract_method_call) end

local fmt, v2s = string.format, tostring

-------------------------------------------------------------------------------

-- debugging print
local ok_dprint, D = pcall(require, 'dprint')
-- make stubs
if not ok_dprint then
  print('dprint Not Found')
  D = {
    enable = function(on) return on end,
    enable_module = function(module, on) return module, on end,
    disable = function(on) return on end,
    mk_dprint_for = function() return function() end end,
    set_silent_print = function() end,
    get_messages = function() return {} end,
    _VERSION = 'stub',
  }
end
-- to enable dprint for this module use D.enable_module('oop.base')
local dprint = D.mk_dprint_for(M, false)
M.dprint = dprint
M.D = D

-------------------------------------------------------------------------------

-- all registered classes
local classes = {}

local next_id = 0

-- the current package (namespace) for a new defined class|interface|enum
-- UsageExample:
--  class.package 'io.app'              -- set package name
--  local MyClass = class.MyClass {...  -- use package name for ClassName
--  return class.build(MyClass)         -- clear package name
M.package_name = ''

--
-- unnamed (default) class name based id
--
---@param superclass_name string? -- name of the parent class to inherit
---@param ctype number? -- base{CLASS|INTERFACE|ENUM}
function M.next_class_name(superclass_name, ctype)
  next_id = next_id + 1

  local prefix = superclass_name or 'pkg.'
  local typename = M.CTYPE[ctype or M.CLASS] or M.CTYPE[M.CLASS]
  return prefix .. '_' .. typename .. tostring(next_id)
end

--
-- Register new cleated class
-- To be able to access a class by its name
--
---@param o oop.Object|oop.Interface
---@return oop.Object|oop.Interface
function M.register(o)
  dprint(1, 'register class', o)
  assert(type(o) == 'table', 'class|interface must be a table')

  local cn = rawget(o, '_cname')
  assert(type(cn) == 'string', '_cname must be a string has:' .. type(cn))

  local ct = rawget(o, '_ctype')
  assert(ct == M.CLASS or ct == M.INTERFACE or ct == M.ABSTRACT_CLASS or
    ct == M.ENUM, 'class|interface|enum expected has ctype:' .. tostring(ct)
  )

  -- TODO settings to override old
  assert(not classes[cn], 'already registered "' .. tostring(cn) .. '"')

  classes[cn] = o
  return o
end

---@param name string -- class or interfaces name
---@return boolean
---@param cascade boolean? todo
function M.unregister(name, cascade)
  if type(name) == 'string' and name ~= M.ObjectClassName then
    local o = classes[name]
    if cascade then
      -- todo remove all classes used this one
      error('Not Implemented yet')
    end
    classes[name] = nil
    return o ~= nil
  end
  return false
end

--
-- remove all registered classes and interfaces
--
function M.unregister_all()
  local object = classes[M.ObjectClassName]
  classes, next_id = {}, 0
  M.register(object)
end

-- return copy of all registered classes
---@return table
function M.get_classes()
  local t = {}
  for name, o in pairs(classes) do
    -- TODO readonly for built classes
    t[name] = o
  end
  return t
end

--
-- get class itself by his full name (with package)
-- if the class is not loaded and can be loaded (can_load == true) then
-- load it as a regular lua-module via given name
--
---@param name string
---@param can_load boolean? if class not loaded try to require by its name
---@return oop.Object|oop.Interface|nil
function M.class_for(name, can_load)
  if type(name) == 'string' then
    local klass = classes[name]
    -- try to load as regual lua-module (on success it will register itself)
    if not klass and can_load then
      local ok, res = pcall(require, name)
      if ok and type(res) == 'table' then
        klass = res
      end
    end
    return klass
  end
end

--
--
function M.ensure_with_package(name)
  local pkg_name = M.package_name
  if name and pkg_name and pkg_name ~= '' and not name:find('.', 1, true) then
    name = pkg_name .. '.' .. name
  end
  return name
end

function M.is_valid_classname(name)
  if type(name) == 'string' and #name > 0 and name:match("^[%a%d%_%.%$]+$") then
    local fc = name:sub(1, 1)
    if fc:match("[%a]") then
      -- ClassName or io.pkg.ClassName
      return string.upper(fc) == fc or name:find('%.%u') ~= nil
    end
  end
  return false
end

--
-- get Class of the instance
--
---@param instance any|table?
---@return table?
function M.get_class(instance)
  return type(instance) == 'table' and getmetatable(instance) or nil
end

--
-- oop.Object  ->  Object
--
---@param classname string|table|nil
---@return string|nil
function M.get_short_class_name(classname)
  local t = type(classname)
  if t == 'table' then
    classname = classname._cname
  end
  if type(classname) == 'string' then
    return classname:match('([%w]+)$')
  end
  return nil
end

--
-- extract from a given full class name the package name
--
---@param classname string?
---@return string?
function M.get_package_name(classname)
  local scn = M.get_short_class_name(classname)
  if scn and classname and #scn < #classname then
    return classname:sub(1, #classname - #scn - 1)
  end
  -- return classname and classname:match("(.*[%.])%.") or classname
end

--
-- join optional package with short-classname with given ns separator (dot)
--
---@param pkg string?
---@param short_classname string
---@param ns_sep string? default is '.'
function M.mk_full_classname(pkg, short_classname, ns_sep)
  ns_sep = ns_sep or '.'

  if pkg and pkg ~= '' then
    return v2s(pkg) .. ns_sep .. v2s(short_classname)
  end
  return short_classname
end

-- -- class can be just a class, interface, enum,  etc.
-- ---@param value any
-- ---@param ctype number
-- function M.get_class_type(value, ctype)
--   ctype = ctype or M.CLASS -- ?
--   return ctype == M.CLASS and M.is_class(value) or
--       ctype == M.INTERFACE and M.is_interface(value) or
--       ctype == M.ENUM and M.is_enum(value)
-- end

---@return string?
function M.getClassName(self)
  return type(self) == 'table' and self._cname or nil
end

--
-- checks is given value is a certain Class (not instance of the some Class)
--
---@param value any
---@return boolean
function M.is_class(value)
  if type(value) == 'table' then
    local ct = rawget(value, '_ctype')
    return (ct == M.CLASS or ct == M.ABSTRACT_CLASS)
        and getmetatable(value) ~= nil or
        rawget(value, '_cname') == M.ObjectClassName
  end
  return false
end

-- for debugging
function M.is_defined_class(value)
  return M.is_class(value) and not rawget(value, 'extends')
end

--
-- check whether the given value is an object(instance) of a some class
--
---@param value any
function M.is_object(value)
  return type(value) == 'table' and
      rawget(value, '_ctype') == nil and M.is_class(getmetatable(value))
end

--
--
-- check whether the given value is an interface
-- that describes a set of classes to implement
--
---@param value any
function M.is_interface(value)
  return type(value) == 'table' and
      rawget(value, '_ctype') == M.INTERFACE and
      rawget(value, '_cname') ~= nil
end

--
-- check whether the specified parent is in the child's inheritance hierarchy
-- child is a ancestor of parent or "is A related to B"
--
-- (technically: is parent on of metatable existed in child-table)
--
---@param child table
---@param parent any
local function is_inherited_mt(child, parent)
  if child ~= nil and parent ~= nil then
    local i = 1
    while type(child) == 'table' and i < M.max_inherit_deep do
      local parent0 = getmetatable(child) -- getClass or getParentInterface
      if parent0 == parent then
        return true
      end
      child = parent0
      i = i + 1
    end
  end
  return false
end

--
-- Check is current member is an inheritor of the given klass
--
-- member can be a class, instance, or interface
-- klass can be a class or interface(for interface memeber only
--
-- works both for instancies, classes and interfaces
--
-- UsageExample:
--
-- instancies and class inheritance:
--     MyClass:instanceof(Object)
--     instance:interfaceof(MyClass)
--
-- interface inheritance:
--     IEventListener:instanceof(IListener)
--     BusClass:instanceof(IEventListener)
--
---@param klass oop.Object|oop.Interface|table|string?
---@return boolean
function M.instanceof(member, klass) -- Object.instanceof(instance, class)
  if type(klass) == 'string' then
    ---@diagnostic disable-next-line: cast-local-type
    klass = M.class_for(klass)
  end

  if M.is_class(member) or M.is_object(member) then
    if M.is_class(klass) then
      return is_inherited_mt(member, klass)
    elseif M.is_interface(klass) and member._implements then
      return member._implements[klass] == true
    end
    --
  elseif M.is_interface(member) and M.is_interface(klass) then
    return is_inherited_mt(member, klass)
  end

  return false
end

--
--
--
---@param value any
function M.is_enum(value)
  return type(value) == 'table' and rawget(value, '_ctype') == M.ENUM
end

-- used to define methods signatures in the interface and
-- todo abstract classes
function M.is_abstract_method(value)
  return value == M.abstract_method or M.is_method_signature(value)
end

-- is a given value is a some type itself string|number|table|oop.Class
function M.is_type(v)
  return v ~= nil and (
    v == string or v == table or
    v == 'number' or v == 'string' or v == 'table' or v == 'function' or
    v == 'boolean' or -- or v == 'any' or 'void'
    v == M.ObjectClassName or
    M.is_class(v) or M.is_interface(v)
  )
end

--
-- readable info about object, class, interface or MethodSignature
-- for debugging
-- todo
-- show all interface methods (MethodSignature)
--
---@param o any
---@return string
function M.inspect(o)
  local t, s, i = o, '', 0

  while type(t) == 'table' and i < M.max_inherit_deep do
    local ctype = rawget(t, '_ctype')
    local cname = rawget(t, '_cname')
    local stype = '?'

    if type(ctype) == 'number' then
      if ctype == M.METHOD_SIGNATURE and o.__tostring then
        return 'method signature: ' .. tostring(o)
      end
      stype = (M.CTYPE[ctype] or '?'):lower()
    elseif s == '' then
      if M.is_object(o) then -- check first table is a instance
        stype = 'object'
      elseif not next(t) then
        stype = '{}' -- empty table
      end
    end
    s = s .. stype
    if cname then
      s = s .. '(' .. tostring(cname) .. ')' -- type name for class
    end

    t = getmetatable(t)          -- class or parent
    if t then s = s .. ' : ' end -- inheritance
    i = i + 1
  end

  if s == '' then
    s = tostring(o)
  end

  return s
end

-- nil -> "void"
-- srting -> "string"
--
---@param _type any
---@param fullclassname boolean?
---@return string
function M.type_tostring(_type, fullclassname)
  if _type == nil then
    _type = 'void'
  elseif _type == string then
    _type = 'string'
  elseif _type == table then
    _type = 'table'
  elseif type(_type) == 'table' then
    local cname = rawget(_type, '_cname')
    -- is class | interface | enum
    if cname then
      cname = fullclassname and cname or M.get_short_class_name(cname)
      _type = cname or _type
    end
  end
  return tostring(_type)
end

--
---@param v any
---@return boolean
function M.is_method_signature(v)
  return type(v) == 'table' and rawget(v, '_ctype') == M.METHOD_SIGNATURE
end

--
-- copy definition - all "public" fields and method that not starts with "_"
--
-- used at the class definition stage
--
---@param dst table
---@param src table
function M._copy_def(dst, src)
  for k, v in pairs(src) do
    if (not M.LIB_KEYS[k] or k == '_init' or k == '__tostring') then
      dst[k] = v
    end
  end
end

--
-- the default tostring method
-- intended for displaying a unique hash of a given value
-- if value is a class - add prefix "class"
--
-- for __tostring
---@return string
function M._tostring(v)
  dprint(1, "_tostring")

  if v then
    local vtype, pref = type(v), ''

    -- or Object.instanceof(v, Object) then
    if vtype == 'table' then
      if v._cname then
        vtype = v._cname
        local ctype = M.CTYPE[rawget(v, '_ctype') or false]
        if ctype then pref = ctype:lower() .. ' ' end
      end
      if type(v.toString) == 'function' then
        v = v:toString() or '?'
        --
      elseif v.__tostring == M._tostring then
        v = '@' .. M._get_table_hash(v)
      end
    end

    return pref .. '(' .. vtype .. ') ' .. tostring(v)
  end
  return tostring(v)
end

--
-- TODO find way to get table "hash" without reset metatable
--
---@param t table
---@return string
function M._get_table_hash(t)
  -- a trick to avoid looping if has __tostring
  local temp, s = getmetatable(t), nil
  setmetatable(t, nil)  -- debug.setmetatable
  s = tostring(t)
  setmetatable(t, temp) -- back

  s = s:gsub('table: 0x', '', 1)
  return s
end

--------------------------------------------------------------------------------
--                       Serialize & Deserialize
--------------------------------------------------------------------------------
-- interface:
M.serialize_helper = {}
local SH = M.serialize_helper
--[[
local SH = base.serialize_helper
local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id
local get_class_id = SH.get_class_id
local get_class_name = SH.get_class_name
local add_class_ref = SH.add_class_ref
local invert_map = SH.invert_map
]]

-- to starts with oop.Object == 1
-- Why:
-- this is done because when serializing classes that inherit from oop.Object,
-- this superclass will not be placed first(with id == 1).
-- But it won't work without adding it either. in order for all fields in the
-- entire inheritance tree to be serialized, you need to reach the root "Object"
-- superclass.
-- Therefore, even if the serialized data does not directly contain instances
-- of this superclass(oop.Object), it will still appear in the map of all used
-- classes.  That's why, for nice look, I put it here first
--
---@return table
function SH.new_cname2id()
  local cname2id = {}
  SH.get_class_id(cname2id, M.ObjectClassName)
  return cname2id
end

--
-- register new or return already registered class_id for given classname
-- in the given cname2id(table)
--
---@param cname2id table
---@param cname string
---@return number
function SH.get_class_id(cname2id, cname)
  assert(type(cname) == 'string', 'cname')
  local id = cname2id[cname]
  if not id then
    id = cname2id.next_id or 1
    cname2id[cname] = id
    cname2id.next_id = id + 1
  end
  return id
end

--
-- return class name from sirialized object by t._class or by class_id via
-- id2cname[t._clsid] if id2cname presended (obtain via invert_mape(cname2id))
--
---@param t table{_class|_clsid} - serialized object
---@param id2cname table?
---@return string?
function SH.get_class_name(t, id2cname)
  local classname = t._class

  if id2cname then
    if not classname and t._clsid and id2cname then
      classname = id2cname[t._clsid]
    end
    if not classname then
      error(fmt('Not Found classname: _class:%s _clsid:%s has id2cname:%s',
        v2s(t._class), v2s(t._clsid), v2s(id2cname ~= nil)))
    end
  end

  return classname
end

--
-- add one of _clsid or _class to given t(serialization table)
--
-- used in the stage of serialization (in Object.toArray)
--
---@param t table
---@param klass table{_cname:string}
---@param cname2id0 table?
---@return table
function SH.add_class_ref(t, klass, cname2id0)
  assert(type(klass) == 'table' and klass._cname, 'expaced klass._cname')

  local classname = klass._cname
  if cname2id0 then
    assert(classname, 'expected klass._cname')
    t._clsid = SH.get_class_id(cname2id0, classname)
  else
    t._class = classname
  end

  return t
end

--
-- create the map used for deserialize back to instance
--
-- cname2id -> id2cname    drop next_id
-- id2cname -> cnam2id     without next_id (broken?)
function SH.invert_map(cname2id)
  local t = {}
  for cname, id in pairs(cname2id) do
    if cname ~= 'next_id' then
      t[id] = cname
    end
  end
  return t
end

---@param obj oop.Object
function SH.clone_with_new_class(obj)
  local Object = classes[M.ObjectClassName]
  if Object then
    local t = Object.toArray(obj, {}, true, nil)
    return obj.fromArray(nil, t, true, nil)
  end
end

--------------------------------------------------------------------------------

if _TEST then
  -- setup test alias for private elements using a modified name
  M._clear_state = function()
    M.unregister_all()
  end
end

return M
