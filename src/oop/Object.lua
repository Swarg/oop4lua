-- 20-09-2023 @author Swarg
--
-- The base class from which all other classes inherit
--

local base = require("oop.base")
local dprint = base.dprint
---@diagnostic disable-next-line: unused-local
local fmt, v2s = string.format, tostring

--
---@class oop.Object
---@field _cname string
---@field _init fun(self:oop.Object, ...)
---@field super fun(self:oop.Object, ...):oop.Object -- available only in _init (constructor)
--
-- methods available only in Class-Defintion stage:
---@field extends    fun(self:oop.Object, parent:oop.Object):self
---@field implements fun(self:oop.Object, ...:oop.Interface):self
--
-- private:
---@field _implements table?  -- list of all implemented interfaces
local Object = {}

-- bind table with functions for class to make sure inherited class can access
-- to methods of the Object class
Object.__index = Object              -- for case then works without calling the Object:new()

Object._ctype = base.CLASS           -- completly defined(sealed) class
Object._cname = base.ObjectClassName -- class name 'oop.Object'
Object.__tostring = base._tostring   -- define method for call in tostring(obj)
Object.getClassName = base.getClassName


--
-- Create new instance of a class
-- with Constructor(_init)
--
--
-- Designed so that this method is not used to declare a new class
-- Since this will automatically call the default constructor (_init)
-- See: ./spec/oop/class_spec.lua "Object:new() vs class.new_class"
--
-- To create a new class, use:
--
-- local class = require('oop.class')
-- local MyClass = class.MyClass :extends(Object) {
--   field =.. ,
--   _init = function(self, ...) ... end,
-- }
--
--    or
--
-- local MyClass = class.new_class('MyClass', Object, {
--   field = ... , ...
-- })
--
--
---@generic T
---@param self `T`      -- the type is captured using `T`
---@param cdef table?   -- the class definition (fields, methods)
---@return T            -- generic type is returned-
function Object:new(cdef, ...) -- function Object.new(Object, o)
  cdef = cdef or {}
  self.__index = self          -- Object.__index = Object
  -- the __call is not set here because if you set it here you
  -- can call the instance as a function.
  -- And this is not the behavior that is expected from the object
  setmetatable(cdef, self)

  cdef:_init(...) -- call Constructor

  return cdef
end

--
-- just for clarity and understanding of how it works under the hood:
--
-- function Object.new(self_class, o)   -- function Object:new(o)
--   o = o or {}
--   self_class.__index = self_class    -- Object.__index = Object
--   setmetatable(o, self_class)        -- setmetatable(o, Object)
--   sefl_class._init(o, ...)           -- Object._init(o, ...)
--   return o
-- end


--
-- Constructor
--
-- this is a default Constructor for instantiate a new object from the Class
-- automatically fires on call Class:new(params) or Class(paramas)
---@vararg any
function Object:_init(...)
  -- self. ...
end

--
-- "On Call Class"
--
-- UseCases:
--   - add the definition of the class via call table:   MyClass { class_def }
--     ( on the "class-defintion" stage before class.build()
--   - create a new object from class:                   local obj = MyClass()
--
-- class_def (class definition) - are fields and methods
--
---@generic T
---@param self `T` | oop.Object
--- overload fun(self, v1:table):T
---@return T self
function Object.__call(self, ...)
  -- dprint(1, '>> Object.__call')

  -- quickly without fully checking whether the passed value is a class
  local ctype = rawget(self, '_ctype')
  local defined = not rawget(self, 'extends') -- finished

  -- check is current class is completed. defined class does not have this method
  if ctype == base.CLASS and defined then
    dprint('create new Instance of:', self._cname)
    return self:new(nil, ...)
  end

  if not defined and (ctype == base.CLASS or ctype == base.ABSTRACT_CLASS) then
    local arg_cnt = select('#', ...)
    dprint('self is a Class', self._cname, 'arg cnt:', arg_cnt)

    -- MyClass({fields}) or same MyClass{fields} -- add class definition
    if arg_cnt >= 1 then
      local cdef = ... -- select(1, ...)
      assert(type(cdef) == 'table', 'class definition must be a table')

      base._copy_def(self, cdef)
    end
    --
  elseif ctype == base.ABSTRACT_CLASS and defined then
    error('attempt to instantiate an abstract class:' .. tostring(self))
  else
    error('attempt to call a table of Instance. ctype:'
      .. tostring(ctype) .. ' ' .. tostring(self))
  end

  -- dprint('<< Object.__call')
  return self
end

--
-- Class of current instance (can used to instantiate a new objects)
function Object:getClass()
  -- case: trying to get a Class of Class(Not Instance of Object) ??
  return getmetatable(self)
end

--
-- Check is current instance or class is an inheritor of the given class
-- works both for instancies and classes
--
-- UsageExample:
--   MyClass:instanceof(Object)
--   instance:interfaceof(MyClass)
--
-- See base.instanceof
-- self: table|oop.Object|oop.Interface
---klass: oop.Object|oop.Interface|table|string?
-- return boolean
Object.instanceof = base.instanceof -- Object.instanceof(self, klass)

--
--
---@return string
function Object:hash()
  return base._get_table_hash(self)
end

--------------------------------------------------------------------------------
--                       Serialize & Deserialize
--------------------------------------------------------------------------------

local SH = base.serialize_helper
local get_class_name = SH.get_class_name
local add_class_ref = SH.add_class_ref

-- there are three serialization options
-- 1. serialize an object into a simple array without storing class data
--    serialize == false
-- 2. for each serialized file, save the full name of its class
--    without cname2id and id2cname
-- 3. serialize using a class map in order to store for each serialized object
--    only the class_id stored in the classes map, in order to store only
--    the corresponding ID (numbers) instead of full class names.
--   (Uniqueness of identifiers within the context of one serialization.)

--
-- deep-copy of all fileds without a Classes (metatables)
--
-- serialize == true to serialize to lua-table
-- (add (string)_class - the names of class of each object for deserializing)
--
-- usefull for debugging
-- all values without Class
--
-- Note: Classes which extends oop.Object keeps its names in _cname but
-- in serialized table its names holds in "_class" or in "_clsid"
--
---@param self table
---@param t table? -- container to serialize
---@param serialize boolean? - flag to keep classes names
---@param cname2id table? - classname to id in a context of one serialization
---@return table
---@param passed table?
function Object.toArray(self, t, serialize, cname2id, passed)
  assert(type(self) == 'table', 'self must be a table')
  assert(not t or type(t) == 'table', 't must be a table or nil')

  -- protection against circular references and stack overflow
  passed = passed or {}
  if passed[self] then return passed[self] end

  -- recursively collect all fields from all inherited classes
  t = t or {}
  passed[self] = t -- mark as passed

  local klass = getmetatable(self)
  if klass then
    if not passed[klass] then
      Object.toArray(klass, t, serialize, cname2id, passed) -- inheritance
      assert(passed[klass], 'sure marked as passed')
    end
    if (serialize or cname2id) then
      add_class_ref(t, klass, cname2id)
    end
  end

  ---
  for k, v in pairs(self) do
    -- k ~= '__index' then
    local vt = type(v)
    if v ~= klass and vt ~= 'function' and not base.LIB_KEYS[k] then
      if vt == 'table' then
        if not passed[v] then
          -- deep copy without metatables
          v = Object.toArray(v, {}, serialize, cname2id, passed)
        else
          v = passed[v]
        end

        -- local cls = getmetatable(v)
        -- if (serialize or cname2id) and cls then
        --   add_class_ref(v, cls, cname2id)
        -- end
      end
      t[k] = v
    end
  end

  return t
end

--
-- deserialize from lua-table
--
-- build the instance from given table without calling a constructor
--
---@param self oop.Object? - class
---@param t table          - object fields (serialized data)
---@param strict boolean? -- if true then throw error on classname not found
---@param id2cname table?
---@param passed table?
function Object.fromArray(self, t, strict, id2cname, passed)
  assert(type(t) == 'table', 'expected table t')

  -- Goal: Defence to avoid possible infinity loop with metatables
  if getmetatable(t) then
    local cn = t._cname --base.getClassName(t)
    if base.is_class(t) then
      error('arg#2 expected simple table got class: ' .. v2s(cn))
    elseif base.is_object(t) then
      error('arg#2 expected simple table got instance of class: ' .. v2s(cn))
    else
      error('arg#2 expected simple table without metatable')
    end
  elseif t == Object then
    error('arg#2 expected simple table got class: oop.Object')
  end

  strict = strict == nil or strict -- default is true

  -- protection against circular references and stack overflow
  passed = passed or {}
  if passed[self] then return passed[self] end

  ---@param classname string?
  ---@param key string?
  local function get_class(classname, key)
    assert(classname, 'expected classname')
    local klass0 = base.class_for(classname)

    if strict and (not klass0 or not base.is_class(klass0)) then
      local msg = string.format('Not Found class "%s"', tostring(classname))
      if key then msg = msg .. ' for field "' .. key .. '"' end
      error(msg, 3)
    end
    ---@cast klass0 oop.Object
    return klass0
  end

  local cdef = {}
  passed[t] = cdef

  -- build class itself:
  --   this stage may not exist when inside the object there is a list (table)
  --   consisting of other objects
  if self or t._class or t._clsid then
    local classname = get_class_name(t, id2cname)

    if classname and (not self or classname ~= self._cname) then
      self = get_class(classname, 'root')
    end

    if self then
      self.__index = self
      setmetatable(cdef, self)
    end
  end

  for k, v in pairs(t) do
    dprint('k:', k, ':dump:', v)
    if k ~= '_class' and k ~= '__index' and k ~= '_clsid' then
      local vt = type(v)
      if vt == 'table' then
        local klass = nil
        if v._class or v._clsid then
          klass = get_class(get_class_name(v, id2cname), k)
        end
        if not passed[v] then
          -- with case objects inside plain table
          v = Object.fromArray(klass, v, strict, id2cname, passed)
        else
          v = passed[v]
        end
      end
      cdef[k] = v -- copy
    end
  end

  return cdef
end

return Object
