-- 17-11-2023 @author Swarg
--
-- Factory to provide a new Class
-- access (index) to table of this module generate a new class
--

local base = require("oop.base")
local Object = require("oop.Object")
local interface = require("oop.interface")
local method_signature = require("oop.MethodSignature")

local dprint = base.dprint

base.register(Object)

-- class-factory not a oop.Class itself
---@class oop.class_factory {__index: oop.class_factory}

--
-- Creating a new class
-- Designed so that each new class is an inheritor from the base "Object" class
--
-- local MyClass = class.MyClass
-- or
-- local MyClass = class('MyClass')
--
--
-- local SuperClass = class.SuperClass
-- local SubClass = class('SubClassName', SuperClass)
--
-- local SubClass = class.SubClassName:extends(SuperClass) TODO
--
-- here SubClass extends(inherit) SuperClass

local M = {}
local Class = {}

M._URL = 'https://gitlab.com/lua_rocks/oop'
M._VERSION = 'oop 0.6.0'

-- used to create class|interface|enum via __index access:
-- Example:
--   class.interface.Listener
M.next_ctype = base.CLASS

M.is_class = base.is_class
M.is_object = base.is_object
M.is_interface = base.is_interface
M.instanceof = base.instanceof

M.get_class = base.get_class
M.get_short_class_name = base.get_short_class_name
M.get_short_class_name = base.get_short_class_name
M.get_package_name = base.get_package_name
M.is_abstract_method = base.is_abstract_method
M.abstract_method = base.abstract_method -- stub for interfaces

M.serialize_helper = base.serialize_helper

-- for class|interface|enum
M.is_valid_classname = base.is_valid_classname

-- superclass for all new classes
M.Object = Object

-- delegate to interface factory
M.interface = interface

--
M.base = base

-- get an already registered Class by its full name
M.forName = base.class_for

M.tostring = base._tostring

-- set package name (namespace) for current context
-- this name will be used to a new defined Class
function M.package(name)
  base.package_name = name
end

--
-- create a new Class inherit given Base class
--
-- is a inner method for under the hood using
--
---@generic T
---@param superclass `T`?      the parent-class for the new one
---@param classname string?    the name of the new class
---@param cdef table?          definition of the class (fields and methods)
---@return T & oop.Object      generic. a new type derived from the superclass
---@vararg table<oop.Interface>? - implements
function M.new_class(superclass, classname, cdef, ...)
  dprint(">> new_class")
  assert(not superclass or base.is_class(superclass), 'superclass class expected')
  assert(not cdef or type(cdef) == 'table', 'class-definition')
  assert(not classname or M.is_valid_classname(classname),
    'invalid class name: ' .. tostring(classname))

  superclass = superclass or Object
  classname = classname or base.next_class_name(superclass._cname)
  classname = base.ensure_with_package(classname)

  local class = cdef or {} -- new class
  class._ctype = base.CLASS
  class._cname = classname
  class.__index = class        -- to keep access to methods from parent
  class.__call = Object.__call -- to keep ability to call MyClass({def})
  class.__tostring = rawget(superclass, '__tostring') or base._tostring

  -- methods available on class-definition stage
  rawset(class, 'extends', Class.extends)
  rawset(class, 'implements', Class.implements)


  dprint("new_class name:", classname, 'parent:', superclass._cname)
  class = setmetatable(class, superclass) -- return class

  if select('#', ...) > 0 then            -- has implements (interfaces)
    Class.implements(class, ...)
  end

  base.register(class) -- return class
  return class
end

--
--  Depending on the context of the call:
--  1) determine the actual class, from which the _init constructor was called.
--  2) determine the superclass for the founed class (called _init constructor
--     and call constructor (_init) of the superclass for specific context.
--
-- The difficulty here is that the self has its own class,
-- and even being passed to the constructors of the parent classes
-- the search for the superclass will begin not from the calling context,
-- but from its own class with the self variable.
-- i.g. simple class.super = superclass._init will not work correctly
--
-- self.super(params) --> self:getClass():getClass()._init(params)
--
-- WARN. Design to run only from _init function by instance of the some class
--
---@param self oop.Object -- the instance of a some class
---@param ... any         -- params for _init constructor
function Class.wrapped_super(self, ...)
  dprint('wrapped_init self(instance):', self)

  local klass = getmetatable(self)

  -- to fast check without debug.getinfo
  -- cases of shallow inheritance
  -- if klass then
  --   local superclass = getmetatable(klass)
  --   if superclass == Object then
  --     Object._init(self, ...)
  --   elseif getmetatable(superclass) == Object then
  --     superclass._init(self, ...) -- todo check
  --   end
  --   return
  -- end

  --
  -- 1 find the class from which _init is called

  local _init
  -- function from which this code was called(_init)
  local caller = debug.getinfo(2, 'f').func

  while klass do
    _init = rawget(klass, '_init')
    if _init == caller then
      dprint('Found Class of the Caller:', klass._cname)
      break
    end
    klass = getmetatable(klass) -- get the parent of current class
    dprint('Checked :', (klass or {})._cname)
  end

  -- 2 find the class of next _init (super)
  while klass do
    klass = getmetatable(klass)
    _init = rawget(klass, '_init')
    if _init then
      dprint('Found SuperClass with _init: ', klass._cname)
      break
    end
    dprint('Checked:', (klass or {})._cname)
  end

  -- 3 call _init of founded Class
  if klass and _init then
    if _init then
      dprint('call _init from:', klass._cname)
      _init(self, ...)
    end
  end
end

--
--
---@param parent oop.Interface?
---@param name string?    the name of new interface
---@param def table?     methods of the interface
---@return oop.Interface
function M.new_interface(parent, name, def)
  return interface.new(parent, name, def)
end

--
-- Create a new method signature (function signature)
-- used to define interface or abstract class
-- Example:
--  handle = interface.method(EventListener, 'self', Event, 'event'),
--
M.func = method_signature.new -- ? abstract func ?
M.new_method_signature = M.func



-- local class = require 'oop.class'
-- extends = class.extends
-- implements = class.implements
--
-- not a fully realized idea:
-- TODO
-- local MyClass = class('MyClassName', extends, BaseClass, implements, Interface)
--
local function create_new_class(...)
  local arg_cnt = select('#', ...)
  local classname, superclass

  if arg_cnt == 1 then
    local arg = ...               -- select(1, ...)
    if type(arg) == 'string' then -- classname
      classname, superclass = arg, Object
    elseif M.is_class(arg) then
      classname, superclass = base.next_class_name(), arg
    end
    --
  elseif arg_cnt == 2 then
    local arg1, arg2 = ...

    if type(arg1) == 'string' and M.is_class(arg2) then
      classname, superclass = arg1, arg2
    elseif type(arg2) == 'string' and M.is_class(arg1) then
      classname, superclass = arg2, arg1
    else
      error('Wrong arguments to create a new class 1:'
        .. type(arg1) .. ' 2:' .. type(arg2))
    end
  else
    error('Expected 1 or 2 args, has ' .. arg_cnt)
  end

  return M.new_class(superclass, classname)
end

--
-- for internal use under the hood
--
-- Usage Example:
-- local MyClass = class.extends('org.MyClassName', ParentClass)
--
-- or
--
-- local MyClass = class.new_class('org.MyClass')    -- internal method
-- class.extends(MyClass, ParentClass)
--
---@param parent table             -- The base class from which we inherit
---@param child table|string       -- a new classname or alredy created Class
function M.extends(child, parent)
  parent = parent and parent or Object

  if type(child) == 'string' then
    -- create new class from string name
    return M.new_class(parent, child)
  end

  -- set inheritance
  setmetatable(child, parent)

  return child
end

--
-- Set SuperClass(Parent) to already created SubClass(Child)
-- (Used and should only be available at the class definition stage)
-- usecase:
--    - one-line syntax for class definition
--
-- Usage Example:
--
-- one-line syntax (the syntax for which this method is intended):
--
--   local MyClass = class.MyClass :extends(SuperClass)
--
-- how its work under the hood:
--
--   local MyClass = class.new_class('MyClass')
--   MyClass = MyClass:extends(SuperClass)
--
---@param superclass table   -- The base class from which the inheritance comes
function Class.extends(self, superclass)
  local class = self
  assert(base.is_class(class), base.errmsg_only_for_classes)
  assert(base.is_class(superclass), 'superclass must be a class')

  superclass = superclass and superclass or Object

  -- try to use: Object:extends(Object)  -- ?
  if superclass == Object and self == Object then
    return self
  end

  rawset(self, 'super', superclass._init)

  -- set inheritance
  return setmetatable(class, superclass) -- return class
end

--
-- To declare the interfaces implemented by the class
-- (Used and should only be available at the class definition stage)
--
-- Checks abstract and apply defualt methods.
--
-- Warning, this method modifies the current class
-- (adds defaults methods from given interfaces)
--
-- Detailed:
-- check is a given class implement all specified in given interfaces(tables)
-- abstract mehtods and apply default method into self Class
--
-- if has no implemented method - throw the error with message of a list with
-- not implemented abstract methods
--
--  Object:implements(Listener, Rannable)
--
-- Checking whather all declared interface methods are actually implemented
-- will be called at the end of the class declaration through the class.build()
--
-- Designed so that the check occurs once at the end of the module in which
-- the class is defined
--
---@vararg table|oop.Interface?
---@return self
function Class.implements(self, ...)
  local class = self
  assert(base.is_class(class), base.errmsg_only_for_classes)

  local cnt = select('#', ...)
  assert(cnt > 0, 'expected Interfaces')

  for i = 1, cnt do
    local iface = select(i, ...)
    assert(base.is_interface(iface), 'interface expected')
    self._implements = self._implements or {}
    self._implements[iface] = true

    -- local interface_name = interface._cname or ('Interface' .. tostring(i))

    -- check implemented methods
    for imethod_name, imethod in pairs(iface) do
      if not base.LIB_KEYS[imethod_name] then
        local method = class[imethod_name]

        if not method and not base.is_abstract_method(imethod) then
          -- copy default method from the interface into Class itself
          -- Goal: Replacement of multiple inheritance with default methods
          base.dprint('copy method to class:', imethod_name)
          class[imethod_name] = imethod
        end
      end
    end
  end

  return self -- class
end

--
--
-- on seal class before use
-- can throw error
--
---@param self table -- class inherited from "Object"
function M.check_class_definition(self)
  dprint('check_class_definition ', self, self._implements)
  if self._implements then
    local errmsg = ''
    for iface, _ in pairs(self._implements) do
      assert(base.is_interface(iface), 'interface expected')
      dprint('Check Interface:', iface._cname)

      for imname, m in pairs(iface) do
        dprint('method:', imname, 'in-self:', self[imname])

        if not base.LIB_KEYS[imname] and not self[imname] then
          dprint('not exitst:', imname)

          -- interface method not implemented
          if base.is_method_signature(m) then
            self._implements[iface] = false -- mark as not implemented

            -- message to notify developer about errors in the code
            if #errmsg > 0 then errmsg = errmsg .. "\n" end
            errmsg = errmsg .. 'NotImplemented: ' ..
                tostring(base.get_short_class_name(iface._cname)) .. '.' ..
                imname
            --
          else                        -- is defualt method from interface
            if type(m) == 'function' then
              rawset(self, imname, m) -- copy method to self class
            else
              dprint('ignore not function from interface', imname)
            end
          end
        end
      end
    end

    -- TODO: settings just to complain to log without stopping the work
    if errmsg ~= '' then
      error(errmsg)
    end
  end
  -- assert(self.new ~= nil)
end

--
-- finalize class definition or "close defintion stage"
--
-- Mark a class as fully defined and ready for create a new instances from it
--
-- Two task are solved here:
-- 1) checking whether a given class actually implements all declared interfaces
-- 2) the table class is turned to a state in which its call will create new
-- instances. Previously, calling a class before finalizing it was used to
-- add new fields and methods to it
--
-- (switch __call to the state that create new object from Class.
-- from prev stage that adds definition(fields and methods) to the class)
--
-- UsageExample:
--
--    class.package 'my.app.core'
--
--    local MyClass = class.MyClass :extends(Base) :implements(Listener) {
--       field = 1,
--       _init = function(self, x,y) ... end,  -- constructor
--       getX = function(self) return x end,
--       setX = function(self, x) self.x = x end,
--       ...
--    }
--
--    return class.build(MyClass)                -- "close" the module of class
--
--    --
--    MyClass = require('my.app.core.MyClass')
--    local obj = MyClass()                            -- create new instances
--
---@generic T
---@param klass `T`|oop.Object  --  SubClass from oop.Object
---@return T
function M.build(klass)
  dprint(1, 'finalize class defintion', klass)

  M.next_ctype = base.CLASS -- reset state to create next element
  base.package_name = ''    -- clear for next use

  -- build class
  if not base.is_class(klass) then
    local errmsg = 'build:class expected has: ' .. base.inspect(klass)
    error(errmsg) -- notify developer about in code error
  end

  M.check_class_definition(klass)
  -- definition of the class now is complitly defined
  -- rawset(class, '_ctype', base.CLASS)

  -- remove methods available on class-definition stage
  -- this is a sign that the class declaration is complete
  rawset(klass, 'extends', nil)
  rawset(klass, 'implements', nil)

  -- provide self.super() to run constructor of the superclass in _init
  rawset(klass, 'super', Class.wrapped_super) -- superclass._init)
  assert(klass.super, Class.wrapped_super)

  return klass
end

--
-- return an element (Class, Interface, Instance, MethodSignature) name
-- same as Object.getClassName()
--
---@param o any?
---@return string?
function M.name(o)
  return type(o) == 'table' and o._cname or nil
end

--
-- serialize instance to lua-table (deep-copy)
-- (add (string)_class - the names of class of each object for deserializing)
--
---@param obj oop.Object
---@return table
function M.serialize(obj)
  assert(base.is_object(obj), 'only for instances')
  ---@cast obj oop.Object
  return obj:toArray(nil, true)
end

--
-- deserialize lua-table into instance
-- works based the "_class" fields and registered Classes in base.classes map
--
-- build the instance from given table without calling a constructor
---@param t table
---@return oop.Object
---@param strict boolean? throw error on class not found (def is true)
function M.deserialize(t, strict)
  return Object.fromArray(nil, t, strict)
end

--
--
if _TEST then
  -- setup test alias for private elements using a modified name
  M._get_next_ctype = function()
    dprint('_get_next_ctype ret:', M.next_ctype)
    return M.next_ctype
  end
  M._Class = Class
end


--
--
local class_mt = {
  -- class(), class('MyCalss'),  class('MyClass', Base)
  __call = function(_, ...)
    return create_new_class(...) -- in development
  end,

  -- to create a new Class  via access to the table field
  ---@return oop.Object
  __index = function(self, key) -- class.MyClass
    dprint(1, 'class: access to field:', key)
    -- here key is classname
    self = self

    return M.new_class(Object, key, nil)
  end
}


-- syntax prototype
-- java: public class Class extends Frame implements Listener
-- local ClassA = class.ClassA:extends(Object):implements()

--
-- local MyClass = class.MyClass:extends(Base):implements(Listener)
-- local MyClass = class['pkg.MyClass']:extends(Base):implements(Listener)
--
-- TODO
-- local MyClass = class('pkg.MyClass', extends, Base, implements, Listener)
-- local Listener = interface('Listener', extends, BaseListener)
--

return setmetatable(M, class_mt) -- M
