-- 20-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G._TEST = true

local base = require("oop.base")
local class = require("oop.class");
local interface = require("oop.interface");
local Object = require("oop.Object")

local M = require("oop.MethodSignature");

describe("oop.MethodSignature", function()
  before_each(function()
    base._clear_state()
    base.D.disable()
    -- base.D.enable_module(base, true)
  end)

  --
  -- function or method signature is used to define methods in a interfaces or
  -- in an abstract classes
  -- on call - throw a warning error
  it("new MethodSignature call", function()
    local ms = M.new(string, 'varname')
    assert.is_table(getmetatable(ms))
    assert.is_function(getmetatable(ms).__call)
    assert.same(base.abstract_method, getmetatable(ms).__call)

    assert.match_error(function()
      ms() -- throw an error on attempt to call as function
    end, base.err_abstract_method_call)
  end)

  --
  --
  it("method_singature_tostring", function()
    local s = M.new('self', string, 's', table, 'vn', Object)
    -- local s = interface.method('self', ...)
    -- local s = class.func('self', string, 's', table, 'vn', Object)
    --                                                          return type
    local exp = 'Object func(Object self, string s, table vn)'
    --           ^^return type
    assert.same(exp, M.__tostring(s, false))
    assert.same(exp, tostring(s))
  end)

  -- for static method (without itself object - no first self)
  it("new MethodSignature", function()
    local s = M.new(Object, 'aVarname', 'void')
    --                  type     varname    ^ last is a return type(nil)
    assert.is_table(s)
    assert.same(nil, s['self']) -- sure a static method and not a dynamic one
    assert.same(Object, s['aVarname'])
    assert.same('void', s['return'])
    assert.match_error(function() s(1, 2) end, base.err_abstract_method_call)
  end)

  it("new MethodSignature", function()
    local s = M.new(Object, 'aVarname', ':', 'string')
    --                  type     varname    ^ last is a return type(nil)
    assert.is_table(s)
    assert.same(Object, s['aVarname'])
    assert.same('string', s['return'])
    assert.same(true, base.is_method_signature(s))

    assert.same('string func(Object aVarname)', M.__tostring(s))
    --   return-type^    ^     ^     ^ arg1-name
    --          method-name  arg1-type
  end)

  it("new MethodSignature", function()
    assert.match_error(function()
      M.new('void', 'arg1', 'void', 'arg2')
    end, 'expected simple type or custom Class, got: "void" for vn:"arg1"')
  end)

  -- last param without name is a return type
  it("new MethodSignature number", function()
    local s = M.new('number', 'n', 'string')
    --                        type     vn      ^ return type
    assert.same('number', s['n'])
    assert.same('string', s['return'])
    assert.match_error(function() s() end, base.err_abstract_method_call)
  end)

  --
  -- here
  it("new MethodSignature string table", function()
    local s = M.new('number', 'i', string, 's', Object, 'obj', table)
    --                type     vn   type    vn   type    vn     return type
    assert.same('number', s['i'])
    assert.same(string, s['s'])
    assert.same(Object, s['obj'])
    assert.same(table, s['return'])
  end)

  --
  --
  it("new MethodSignature self", function()
    local s = M.new('self') -- do not need specify type for first "self"
    assert.same(Object, s['self'])

    s = M.new('self', string, 's', Object, 'obj', table)
    assert.same(Object, s['self'])
    assert.same(string, s['s'])
    assert.same(Object, s['obj'])
    assert.same(table, s['return'])

    assert.is_true(base.is_method_signature(s))
  end)

  -- no need to specify the type for the first "self"(will substitute oop.Object)
  -- the second parameter is considered to be the return type
  it("new MethodSignature self+return_type", function()
    -- short form
    local s = M.new('self', 'boolean')
    assert.same(Object, s['self'])
    assert.same('boolean', s['return'])
    assert.is_true(base.is_method_signature(s))

    -- under the hood pevius same as:
    local s2 = M.new(Object, 'self', 'boolean', 'return')
    assert.same(Object, s2['self'])
    assert.same('boolean', s2['return'])
    assert.is_true(base.is_method_signature(s2))

    -- only return can be on place of type for for better code readability
    local s3 = M.new(Object, 'self', 'return', 'boolean')
    assert.same(Object, s3['self'])
    assert.same('boolean', s3['return'])
    assert.is_true(base.is_method_signature(s3))

    -- but not
    local s4 = M.new('return', 'boolean', Object, 'self')
    assert.same(Object, s4['self'])
    assert.same('boolean', s4['return'])
    assert.is_true(base.is_method_signature(s4))
  end)

  -- shortcut for better code readability
  it("new MethodSignature self+return_type", function()
    local s = M.new('self', ':', 'boolean')
    --                              ^ return type
    --                        ^aliase for return
    --
    assert.same(Object, s['self'])
    assert.same('boolean', s['return'])
    assert.is_true(base.is_method_signature(s))
  end)

  it("new MethodSignature self+return_type", function()
    local s2 = M.new('self', string, 's', Object, 'obj', table)
    assert.same(Object, s2['self'])
    assert.same(string, s2['s'])
    assert.same(Object, s2['obj'])
    assert.same(table, s2['return'])

    assert.is_true(base.is_method_signature(s2))

    local s3 = M.new('self', string, 's', Object, 'obj', 'return', table)
    assert.same(Object, s3['self'])
    assert.same(string, s3['s'])
    assert.same(Object, s3['obj'])
    assert.same(table, s3['return'])
    assert.is_true(base.is_method_signature(s3))
  end)

  it("new MethodSignature self+return_type 2", function()
    local s4 = M.new('self', string, 'arg1name', 'return', 'table')
    assert.same(Object, s4['self'])
    assert.same(string, s4['arg1name'])
    assert.same('table', s4['return'])
    assert.is_true(base.is_method_signature(s4))

    local s5 = M.new('self', string, 'arg1name', 'table')
    assert.same(Object, s5['self'])
    assert.same(string, s5['arg1name'])
    assert.same('table', s5['return'])
    assert.is_true(base.is_method_signature(s5))
  end)
  --
  --
  it("method_singature tostring throws", function()
    -- idea for future implementation
    local Throwable = class.Throwable
    local MyException = class.MyException:extends(Throwable)
    local IOException = class.IOException:extends(Throwable)
    local s = M.new('self'):throws(MyException, IOException)
    -- local s = interface.method('self').throws(MyException)

    assert.same(true, base.is_method_signature(s))
    assert.same(false, base.is_interface(s))
    assert.same(false, base.is_object(s))
    assert.same(false, base.is_class(s))
    -- print("[DEBUG] ms:", require'inspect'(ms))

    assert.is_table(rawget(s, '_throws'))
    assert.same(MyException, rawget(s, '_throws')[1])
    assert.same(Object, s.self)


    local exp = 'void func(Object self) throws MyException, IOException;'
    --           ^^return type
    assert.same(exp, M.__tostring(s))
  end)

  --
  it("tostring", function()
    local s = M.new('self', string, 's', Object, 'obj', table)
    local exp = 'table func(Object self, string s, Object obj)'
    assert.same(exp, M.__tostring(s))
  end)

  --
  --
  it("bind actual method name from interface", function()
    local iface = interface.new(nil, 'Runnable', {
      run = M.new('self', string, 's', Object, 'obj', table)
    })
    ---@cast iface table -- to hide diagns
    local exp = 'table func(Object self, string s, Object obj)'
    --                 ^^^^ default method name
    assert.same(exp, M.__tostring(iface.run))

    interface.build(iface)

    local exp2 = 'table run(Runnable self, string s, Object obj)'
    ---                 ^^^ actual metod name from interface
    assert.same(exp2, M.__tostring(iface.run))
  end)

  it("bind find method name from interface", function()
    ---*class Runnable { run: fun(...) }
    local iface = interface.new(nil, 'Runnable', {
      run = M.new('self', string, 's', Object, 'obj', table)
    })
    ---@cast iface table  -- to hide diagnostics
    local ms = iface.run ---@type oop.MethodSignature

    assert.same('run', ms:bind(iface, nil)._cname)
  end)
end)
