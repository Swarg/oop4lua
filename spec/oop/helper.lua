-- 19-11-2023 @author Swarg
-- tooling helper for testing
--
local class = require("oop.class")
local base = require("oop.base")
-- local Object = require("oop.Object")

local M = {}


-- all methods is abstract
---@param name string
---@param ... any -- table<string>
---@return table -- oop.Interface
function M.mk_interface(name, ...)
  local def = {}

  for i = 1, select('#', ...) do
    local v = select(i, ...)
    if type(v) == 'string' then
      local method_name = v
      ---@diagnostic disable-next-line: assign-type-mismatch
      def[method_name] = class.interface.method('self')
    end
  end

  return class.interface.build(class.interface.new(nil, name, def))
end

--
-- self.ret = _cname .. x .. y
--
---@return self
function M.func_self_xy_ret(self, x, y)
  local ts = tostring
  self.ret = self.ret or ''
  self.ret = self.ret .. ts(self._cname) .. ':' .. ts(x) .. ':' .. ts(y)
  return self
end

function M.build_classes(...)
  local cnt = select('#', ...)
  for i = 1, cnt do
    local c = select(i, ...)
    assert(base.is_class(c))
    class.build(c)
  end
end

return M
