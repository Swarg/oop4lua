-- 18-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G._TEST = true
local base = require("oop.base")
local class = require("oop.class");
local Object = require("oop.Object");
local interface = class.interface -- require("oop.interface");

---@diagnostic disable-next-line unused-local
local D = base.D
---@diagnostic disable-next-line unused-local
local inspect = require("inspect")


describe("oop.Interface", function()
  setup(function() end)
  teardown(function() _G._TEST = nil end)

  before_each(function()
    base._clear_state()
  end)

  after_each(function()
    D.disable()
    base._clear_state()
  end)

  -- Note '*class' means '@class' replaced
  -- replaced here so that lua-ls(lsp) doesn't go crazy
  -- Why cast?:
  -- cast an interfaces into a tables so that the lua-ls does not show diagnostics

  it("new_interface", function()
    ---*class MyInterface: oop.Interface
    local MyInterface = interface.MyInterface
    -- local MyInterface = interface.new(nil, 'MyInterface', nil)
    -- local MyInterface = class.new_interface(nil, 'MyInterface')

    assert.is_table(MyInterface)
    assert.same(true, base.is_interface(MyInterface))
    assert.same(false, base.is_class(MyInterface))
    assert.same(false, base.is_object(MyInterface))

    ---*field run function
    MyInterface { run = interface.method('self', Object, 'data') }

    assert.is_true(base.is_interface(MyInterface))
    assert.is_true(base.is_method_signature(MyInterface.run))
  end)

  it("new_interface invalid name", function()
    assert.match_error(function()
      interface.new(nil, '0MyInterface')
    end, 'invalid interface name')
  end)

  --
  -- Note: here i use cast to table to hide lua-ls diagnostics 'undefined-field'
  -- in source code right way is to define fields via nottations with '@'
  -- here i replace it by *class and *field
  it("new_interface with inherit", function()
    ---*class Listener: oop.Interface
    ---*field handle function
    local Listener = interface.new(nil, 'Listener', {
      handle = interface.method() -- base.abstract_method,
    })
    interface.build(Listener)
    ---@cast Listener table  -- to hide lua-ls undefined-field
    assert.same(true, base.is_interface(Listener))
    assert.is_true(base.is_method_signature(Listener.handle))

    -- interface EventListener extends Listener
    ---*class EventListener: Listener
    ---*field post_event function
    local EventListener = class.new_interface(Listener, 'EventListener', {
      post_event = interface.method() -- base.abstract_method
    })
    interface.build(EventListener)
    ---@cast EventListener table  -- to hide lua-ls undefined-field

    assert.same(true, base.is_interface(EventListener))

    assert.is_true(base.is_method_signature(EventListener.post_event))
    assert.is_true(base.is_method_signature(EventListener.handle))

    -- make sure creation of EventListener not affected parent Listener
    assert.is_nil(Listener.post_event)
  end)

  --
  --
  it("new_interface with inherit via :extends()", function()
    local Listener = class.new_interface(nil, 'Listener', {
      handle = interface.method()
    })
    interface.build(Listener)

    local EventListener = class.new_interface(nil, 'EventListener', {
      post_event = interface.method()
    })

    ---@cast EventListener table  -- to hide lua-ls undefined-field
    assert.is_nil(EventListener.handle) -- before inherit
    assert.is_true(base.is_method_signature(EventListener.post_event))

    EventListener:extends(Listener) -- inherit

    assert.is_true(base.is_interface(EventListener))
    assert.is_true(base.is_method_signature(EventListener.post_event))
    assert.is_true(base.is_method_signature(EventListener.handle))

    -- available only in definition stage:
    assert.is_function(EventListener.extends)

    interface.build(EventListener)
    assert.is_nil(EventListener.extends)
  end)

  --
  --
  it("interface inherit via extends + instanceof", function()
    local Listener = class.new_interface(nil, 'Listener', {})
    local EventListener = class.new_interface(nil, 'EventListener', {})

    assert.is_false(EventListener:instanceof(Listener))

    EventListener:extends(Listener)

    assert.is_true(EventListener:instanceof(Listener))
    assert.is_false(Listener:instanceof(Listener))
    assert.is_false(Listener:instanceof(EventListener))
  end)

  --
  --
  it("interface inherit + instanceof", function()
    local Listener = class.new_interface(nil, 'Listener', {})
    local EventListener = class.new_interface(Listener, 'EventListener', {})

    assert.is_true(EventListener:instanceof(Listener))
    assert.is_false(Listener:instanceof(Listener))
    assert.is_false(Listener:instanceof(EventListener))
    assert.is_false(Listener:instanceof(nil))
    local i = {}
    ---@cast i oop.Interface
    assert.is_false(Listener:instanceof(i))
  end)

  --
  --
  it("extends + definition via call", function()
    local Listener = interface.Lis {
      handle = interface.method('self'),
    }
    local EventListener = interface.Elis:extends(Listener) {
      -- method signature
      post_event = interface.method('self'),
    }
    assert.is_true(base.is_method_signature(EventListener.handle))
    assert.is_true(base.is_method_signature(EventListener.post_event))
    -- Listener not affected
    assert.is_true(base.is_method_signature(Listener.handle))
    assert.is_nil(Listener.post_event)
  end)

  --
  -- Replace the default oop.Object by Interface itself
  it("interface.build bind self", function()
    local Event = class.Event {
      state = false
    }
    -- local Listener = class.new_interface(nil, 'Listener') {
    local Listener = interface.Listener {
      handle = interface.method('self', Event),
    }
    assert.same(Object, Listener.handle.self) -- before

    interface.build(Listener)

    assert.same(Listener, Listener.handle.self) -- after build
  end)

  it("__call", function()
    local Listener = interface.new(nil, 'Listener', nil)

    Listener({ run = interface.method() }) -- __call ok

    interface.build(Listener)              -- complete definition

    assert.match_error(function()
      Listener({ anotehr = interface.method() }) -- error
    end, base.err_try_edit_interface)
  end)

  --
  -- default methods copied into Class itself
  it("default method", function()
    -- local Listener = class.new_interface(nil, 'Listener') {
    local Listener = interface.Listener {
      defmethod = function(self, x)
        return tostring(self) .. ':' .. 8 .. tostring(x)
      end
    }
    local MyC = class.new_class(nil, 'MyC', nil, Listener)
    assert.is_true(MyC._implements[Listener])
    assert.is_function(MyC.defmethod)
    assert.same('nil:816', MyC.defmethod(nil, 16))
    class.build(MyC)
    local obj = MyC()
    assert.match('%(MyC%) @[%w]+:816', obj:defmethod(16))

    assert.is_true(obj:instanceof(Listener))
    assert.is_true(obj:instanceof(MyC))
    assert.is_true(obj:instanceof('MyC'))      -- from base.classes
    assert.is_true(obj:instanceof('Listener')) -- from base.classes
    assert.is_true(obj:instanceof(Object))
    assert.is_false(obj:instanceof(class.X))
  end)

  it("getClassName", function()
    class.package 'my.app.api'
    local Listener = interface.Listener
    assert.same('my.app.api.Listener', Listener:getClassName())

    local EventListener = interface.new(Listener, 'EventListener')
    assert.same('my.app.api.EventListener', EventListener:getClassName())

    assert.match('[%w]+', Object.hash(EventListener))
  end)
end)
