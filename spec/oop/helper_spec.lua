require("busted.runner")()
local assert = require("luassert")

_G._TEST = true
local base = require("oop.base")
local class = require("oop.class")
local Object = require("oop.Object")
local H = require('oop.helper')


describe("oop.class", function()
  before_each(function()
    base._clear_state()
  end)

  -- --- check a tooling helper
  it("mk_interface one method", function()
    local Listener = H.mk_interface('Listener', 'run')

    assert.same('Listener', Object.getClassName(Listener))
    assert.same(true, base.is_interface(Listener))
    assert.same(false, base.is_class(Listener))
    assert.same(false, base.is_object(Listener))
    assert.is_true(base.is_method_signature(Listener.run))

    assert.same('run', Listener.run._cname)

    assert.is_table(Listener.run)
    assert.match_error(function()
      Listener.run()
    end, base.err_abstract_method_call)
  end)

  --
  it("mk_interface multiple methods", function()
    local EventListener = H.mk_interface('EventListener', 'run', 'post_event')
    assert.same(true, base.is_interface(EventListener))
    assert.same('run', EventListener.run._cname)
    assert.same('post_event', EventListener.post_event._cname)
    assert.is_true(base.is_method_signature(EventListener.run))
    assert.is_true(base.is_method_signature(EventListener.post_event))
  end)

  --
  it("func_self_xy_ret", function()
    local self = { _cname = 'Class' }
    local exp = { ret = 'Class:8:16', _cname = 'Class' }
    assert.same(exp, H.func_self_xy_ret(self, 8, 16))
  end)

  it("build_classes", function()
    local A = class.new_class(nil, 'A', {})
    local B = class.new_class(A, 'B', {})
    local C = class.new_class(A, 'C', {})

    assert.is_false(base.is_defined_class(A))
    assert.is_false(base.is_defined_class(B))
    assert.is_false(base.is_defined_class(C))
    H.build_classes(A, B, C)

    assert.is_true(base.is_defined_class(A))
    assert.is_true(base.is_defined_class(B))
    assert.is_true(base.is_defined_class(C))
  end)
end)
