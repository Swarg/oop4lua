-- 17-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G._TEST = true
local base = require("oop.base")
local class = require("oop.class")
local interface = require("oop.interface")
local Object = require("oop.Object")

local D = base.D

local D_enable = function()
  D.enable_module(base, true)
end

describe("oop.base", function()
  -- setup(function() _G.test = true end)
  -- teardown(function() _G._TEST = nil end)

  before_each(function()
    base._clear_state()
  end)

  after_each(function()
    D.disable()
  end)

  -- it("dprint", function()
  --   D.enable()
  --   D.dprint('check')
  -- end)

  -- check dprint tooling
  -- D_enable - to enable dprint for this module
  it("dprint D_enable", function()
    if D._VERSION ~= 'stub' then
      D.set_silent_print(true) -- print to messages not to StdOut
      assert.is_nil(base.dprint('check dprint 1'))
      assert.same({}, D.get_messages())

      D_enable()
      assert.same('check dprint 2', base.dprint('check dprint 2'))
      assert.match('check dprint 2', D.get_messages()[1])
      D.set_silent_print(false) -- back
    end
  end)

  it("get_class", function()
    local obj = Object:new()
    assert.equal(Object, base.get_class(obj))
    assert.equal(nil, base.get_class({}))

    local MyClass = { _name = "pkg.MyClass" }
    local obj2 = setmetatable({}, MyClass) -- new instance
    assert.equal(MyClass, base.get_class(obj2))
  end)

  it("is_class", function()
    assert.same(true, base.is_class(Object))
    assert.same(false, base.is_class({}))
    assert.same(false, base.is_class({ _cname = 'X' }))
    assert.same(false, base.is_class({ _cname = 'X', _ctype = base.CLASS }))
  end)

  it("_next_class_name", function()
    assert.same('pkg._Class1', base.next_class_name())
    assert.same('pkg._Class2', base.next_class_name())
    assert.same('pkg._Class3', base.next_class_name())
    base._clear_state()
    assert.same('pkg._Class1', base.next_class_name())
  end)

  it("is_valid_classname", function()
    local f = base.is_valid_classname
    assert.same(true, f('A'))
    assert.same(true, f('ClassName'))
    assert.same(true, f('Class_Name'))
    assert.same(false, f('class_name'))
    assert.same(false, f('pkg.name'))
    assert.same(true, f('pkg.Name'))
    assert.same(false, f('.Name'))
    assert.same(true, f('i.Name'))
    assert.same(true, f('Name'))
    assert.same(false, f('0Name'))
    assert.same(false, f('0.Name'))
    assert.same(false, f('a.0Name'))
    assert.same(true, f('io.pkg.ClassName'))
    assert.same(false, f('.io.pkg.ClassName'))
    assert.same(false, f('_io.pkg.ClassName'))
    assert.same(false, f('io.pkg.Class Name'))
    assert.same(false, f('io.pkg.Class-Name'))
    assert.same(false, f('io.pkg.Class^Name'))
    assert.same(true, f('io.pkg.Class$Name'))
    assert.same(true, f('io$pkg.ClassName')) -- todo fix
    assert.same(false, f('-io.pkg.Name'))
    assert.same(false, f(' io.pkg.Name'))
    assert.same(false, f('io .pkg.Name'))
    assert.same(false, f(' '))
  end)

  it("is_inner_key", function()
    assert.same(true, base.LIB_KEYS['_cname'])
    assert.same(true, base.LIB_KEYS['_ctype'])
    assert.same(true, base.LIB_KEYS['__index'])
    assert.same(true, base.LIB_KEYS['__call'])
    assert.same(true, base.LIB_KEYS['_implements'])
    assert.same(true, base.LIB_KEYS['_throws'])
    assert.is_nil(base.LIB_KEYS['mehtod'])
    assert.is_nil(base.LIB_KEYS[''])
  end)

  --
  -- method used under the hood for syntax like:
  -- local MyClass = class.MyClass {class_def}
  it("on_call under the hood - apply class_def", function()
    local MyClass = class.new_class(Object, 'MyClass')

    assert.is_function(MyClass.__call)
    assert.is_not_nil(rawget(MyClass, '__call'))
    -- assert.is_function(rawget(getmetatable(MyClass), '__call'))

    assert.equal(Object.__call, MyClass.__call)

    assert.is_nil(MyClass.method) -- no this method before apply class_def

    local class_def = {           -- definition of the new class
      ---@diagnostic disable-next-line: unused-local
      method = function(self, x)
        return x
      end
    }
    Object.__call(MyClass, class_def) -- apply class_def to the MyClass
    -- on_call fires on:   MyClass({ method = function ... })

    -- make sure method was added
    assert.is_not_nil(MyClass.method) -- no this method before apply class_def
    assert.same(8, MyClass.method(nil, 8))
  end)

  --
  it("on_call in action", function()
    local MyClass = class.new_class(Object, 'MyClass')

    MyClass { -- definition of the new class
      static_field = 'value',
      static_method = function(x)
        return x
      end
    }
    assert.same(8, MyClass.static_method(8))
    assert.same('value', MyClass.static_field)
  end)

  --
  it("on_call copy _init from class_def", function()
    local MyClass = class.new_class(Object, 'MyClass')

    assert.same(Object._init, MyClass._init)
    assert.is_nil(rawget(MyClass, '_init'))

    local class_def = {
      _init = function(self, x, y)
        self.ret = tostring(x) .. ':' .. tostring(y)
      end,
    }
    Object.__call(MyClass, class_def)

    assert.is_not_nil(rawget(MyClass, '_init'))
    local obj = MyClass:new(nil, 8, 16)
    assert.same('8:16', obj.ret)
  end)

  it("on_call in action constructor", function()
    -- same as class.MyClass {}
    local MyClass = class.new_class(Object, 'MyClass') {
      _init = function(self, x, y)
        self.ret = tostring(x) .. ':' .. tostring(y)
      end,
    }
    local obj = MyClass:new(nil, 8, 16)
    assert.same('8:16', obj.ret)
  end)

  it("on_call in action constructor + auto new", function()
    -- same as class.MyClass {}
    local MyClass = class.new_class(Object, 'MyClass') {
      _init = function(self, x, y)
        self.ret = tostring(x) .. ':' .. tostring(y)
      end,
    }
    -- seal the class to fires MyClass:new(nil, x, y)  on  MyClass(x, y)
    class.build(MyClass)

    local obj = MyClass(8, 16)
    assert.same('8:16', obj.ret)
  end)

  --
  it("_tostring", function()
    assert.same('(string) 123', base._tostring('123'))
    assert.same('(number) 0', base._tostring(0))
    assert.same('(table)', base._tostring({}):match('(%(table%))'))

    -- for classes add prefic "class"
    -- so that you can distinguish an instance (object) from the class
    assert.match('^class %(oop.Object%) @[%w]+', base._tostring(Object))

    local instance = Object:new()
    assert.same('oop.Object', instance._cname)
    -- assert.same('(oop.Object) table: 0x', base._tostring(instance):match(p))
    assert.match('^%(oop.Object%) @[%w]+', base._tostring(instance))
    assert.match('^%(oop.Object%) @[%w]+', instance:__tostring())
    assert.match('^%(oop.Object%) @[%w]+', tostring(instance))

    local ClassA = class.build(class.new_class(Object, 'oop.ClassA'))
    assert.match('^class %(oop.ClassA%) @[%w]+', tostring(ClassA))

    local a = ClassA() -- new ClassA()
    assert.match('^%(oop.ClassA%) @[%w]+', tostring(a))

    local i = interface.new(nil, 'api.Runnable')
    assert.match('^interface %(api.Runnable%) @[%w]+', tostring(i))
  end)

  --
  it("is_type interface", function()
    local Runnable = interface.new(nil, 'Runnable')
    assert.same(true, base.is_interface(Runnable))
    assert.same(true, base.is_type(Runnable))
    assert.same(false, base.is_class(Runnable))
  end)

  it("is_type class", function()
    local MyClass = class.new_class(nil, 'MyClass')
    assert.same(true, base.is_class(MyClass))
    assert.same(true, base.is_type(MyClass))
    assert.same(false, base.is_interface(MyClass))
  end)

  -- it("signature experements", function()
  --   local f, x = base.signature('self', 'param1')
  --   assert.is_function(f)
  --   assert.is_string(x)
  --   assert.match_error(function()
  --     base.signature('self')()
  --   end, base.errmsg_abstract_method_call)
  -- end)


  it("dump_funcs", function()
    if D._VERSION == 'stub' then return end -- works only when dprint installed

    local t = {
      object = 'state',
      tbl = {
        func = base._get_table_hash,
      },
      func = base._get_table_hash,
    }
    t.loop = t
    t = setmetatable(t, { __index = function() end })

    local exp = [[
<table 1 0x[%w]+> {
  tbl = <table 2 0x[%w]+> {
    func = function: 0x[%w]+
  }
  loop = <table 1> {...}
  object = state
  func = function: 0x[%w]+
  <metatable 3 0x[%w]+> {
    __index = function: 0x[%w]+
  }
}
]]
    assert.match(exp, D.dump(t))
  end)

  it("inspect_object", function()
    local f = base.inspect
    assert.same('{}', f({}))
    assert.same('?', f({ k = 9 }))
    assert.same('9', f(9))
    assert.same('class(oop.Object)', f(Object))
    assert.same('object : class(oop.Object)', f(Object:new()))
    assert.same('interface(I) : interface', f(interface.new(nil, 'I')))
    local Listener = interface.new(nil, 'Listener')
    local EventListener = interface.new(Listener, 'EventListenr', {
      post = interface.method('self')
    })
    interface.build(EventListener)

    local exp = 'interface(EventListenr) : interface(Listener) : interface'
    assert.same(exp, f(EventListener))
    ---@cast EventListener table
    local exp2 = 'method signature: void post(EventListenr self)'
    assert.same(exp2, f(EventListener.post))
  end)

  it("unregister_all", function()
    local A = class.new_class(nil, 'A')
    local B = class.new_class(nil, 'B')
    assert.same(A, base.class_for('A'))
    assert.same(B, base.class_for('B'))

    local classes = base.get_classes()
    assert.is_not_nil(classes['A'])
    assert.is_not_nil(classes['B'])

    base.unregister_all()

    classes = base.get_classes()
    assert.is_nil(classes['A'])
    assert.is_nil(classes['B'])

    assert.is_nil(base.class_for('A'))
    assert.is_nil(base.class_for('B'))
  end)
end)

describe("oop.base serialize_helper", function()
  before_each(function()
    base._clear_state()
  end)

  it("clone_with_new_class simple", function()
    local ClassA = class.new_class(Object, 'ClassA')

    local obj_a = ClassA:new({ id = 1 })

    assert.is_table(class.forName('ClassA'))

    assert.equal(ClassA, obj_a:getClass())

    -- emulate hotswap(lua-code reloading)
    assert.same(true, base.unregister('ClassA'))

    assert.is_nil(class.forName('ClassA')) -- sure unloaded

    -- "reload" the code of Classes and recreate instances with new Classes
    local newClassA = class.new_class(Object, 'ClassA')

    assert.equal(newClassA, class.forName('ClassA'))
    assert.are_not_equal(ClassA, class.forName('ClassA'))
    assert.are_not_equal(newClassA, ClassA)

    local new_obj_a = base.serialize_helper.clone_with_new_class(obj_a)
    local exp_obj_a = { id = 1 }
    assert.same(exp_obj_a, new_obj_a)

    assert.equal(newClassA, new_obj_a:getClass())

    -- old object has old Class
    assert.equal(ClassA, obj_a:getClass())
    -- new object with new Class
    assert.are_not_equal(ClassA, new_obj_a:getClass())
  end)


  it("clone_with_new_class cross-refs", function()
    local ClassA = class.new_class(Object, 'ClassA')
    local ClassB = class.new_class(Object, 'ClassB')

    local obj_a = ClassA:new({ id = 1 })
    local obj_b = ClassB:new({ id = 2 })

    obj_a.ref_to_b = obj_b
    obj_b.ref_to_a = obj_a

    assert.is_table(class.forName('ClassA'))

    assert.equal(ClassA, obj_a:getClass())
    assert.equal(ClassB, obj_b:getClass())
    assert.equal(ClassA, obj_b.ref_to_a:getClass())
    assert.equal(ClassB, obj_a.ref_to_b:getClass())

    -- emulate hotswap(lua-code reloading)
    assert.same(true, base.unregister('ClassA'))
    assert.same(true, base.unregister('ClassB'))
    assert.same(false, base.unregister('ClassC')) -- no existed

    assert.is_nil(class.forName('ClassA'))

    -- "reload" the code of Classes and recreate instances with new Classes
    local newClassA = class.new_class(Object, 'ClassA')
    local newClassB = class.new_class(Object, 'ClassB')

    assert.equal(newClassA, class.forName('ClassA'))
    assert.are_not_equal(ClassA, class.forName('ClassA'))
    assert.are_not_equal(newClassA, ClassA)

    local new_obj_a = base.serialize_helper.clone_with_new_class(obj_a)
    local exp_obj_a = {
      id = 1,
      ref_to_b = nil
    }
    local exp_obj_b = {
      id = 2,
      ref_to_a = exp_obj_a
    }
    exp_obj_a.ref_to_b = exp_obj_b
    assert.same(exp_obj_a, new_obj_a)

    assert.equal(newClassA, new_obj_a:getClass())
    assert.equal(newClassB, new_obj_a.ref_to_b:getClass())
    assert.equal(newClassA, new_obj_a.ref_to_b.ref_to_a:getClass())
    assert.equal(newClassB, new_obj_a.ref_to_b:getClass())

    assert.equal(ClassA, obj_a:getClass())             -- old object with old Class
    assert.are_not_equal(ClassA, new_obj_a:getClass()) -- new
  end)

  it("mk_full_classname", function()
    local f = base.mk_full_classname
    assert.same('pkg.Main', f('pkg', 'Main'))
    assert.same('pkg+Main', f('pkg', 'Main', '+'))
    assert.same('Main', f('', 'Main'))
    assert.same('Main', f(nil, 'Main'))
    assert.same('\\pkg\\Main', f('\\pkg', 'Main', '\\'))
  end)
end)
