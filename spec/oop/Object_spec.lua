--
require 'busted.runner' ()
local assert = require('luassert')

_G._TEST = true
local base = require 'oop.base'
local class = require 'oop.class'
local Object = require 'oop.Object'

local H = require('oop.helper')

--
-- NOTE:
-- Why here used class.new_class instead oop.class.new_class:
-- this is done to test this module completely autonomously without
-- using the "oop.class" module
--

local SH = base.serialize_helper
local new_cname2id = SH.new_cname2id
local invert_map = SH.invert_map
local get_class_id = SH.get_class_id


describe('oop.Object', function()
  before_each(function()
    base._clear_state()
    base.D.disable()
  end)

  --
  --
  it("instanceof", function()
    local instance = Object:new()
    assert.is_true(instance:instanceof(Object))
    assert.is_true(instance:instanceof('oop.Object'))
    assert.is_true(Object.instanceof(instance, Object))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.is_false(Object.instanceof(nil, Object))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.is_false(Object.instanceof(nil, nil))
  end)

  it("getClass", function()
    assert.same(Object, Object:new():getClass())

    local MyClass = class.new_class(Object, 'MyClass')
    assert.same(MyClass, MyClass:new():getClass())
  end)

  it("getClassName", function()
    local obj = Object:new()
    assert.same("oop.Object", obj:getClassName())
    assert.same("oop.Object", Object:getClassName())

    local MyClass = class.new_class(Object, 'app.MyClass')
    assert.same("app.MyClass", MyClass:getClassName())
  end)

  it("inherit", function()
    --- ChildClass extends  Object
    local ChildClass = class.new_class(Object, 'ChildClass', nil)
    assert.same('ChildClass', ChildClass._cname)

    -- create instance of Class
    local instance = ChildClass:new() -- new ChildClass() or ChildClass()

    assert.is_true(base.is_class(ChildClass))
    assert.is_false(base.is_class(instance))

    assert.is_false(base.is_object(ChildClass))
    assert.is_true(base.is_object(instance))

    assert.is_true(ChildClass:instanceof(Object))
    assert.is_true(Object.instanceof(ChildClass, Object))

    ---@diagnostic disable-next-line: missing-fields
    assert.is_false(Object.instanceof(ChildClass, {}))

    assert.same(ChildClass, instance:getClass())
    assert.same(true, instance:instanceof(ChildClass))
    assert.same(true, instance:instanceof(Object))

    assert.same('ChildClass', instance:getClassName())
  end)


  --
  it("Object.extends - set superclass for alredy existed class", function()
    ---- create oop.SubClass extends oop.Object
    local SubClass = class.new_class(Object, 'oop.SubClass', nil)

    assert.is_true(base.is_class(Object))
    assert.is_true(base.is_class(SubClass))

    local obj = Object:new()   -- instance of Object class
    local sub = SubClass:new() -- instance of SubClass

    assert.is_false(base.is_class(obj))
    assert.is_false(base.is_class(sub))

    assert.is_true(base.is_object(obj))
    assert.is_true(base.is_object(sub))

    assert.is_true(SubClass:instanceof(Object))
    assert.is_false(Object:instanceof(SubClass))

    local BaseClass = class.new_class(Object, 'oop.Base', nil)
    -- define constructor
    BaseClass._init = function(self, x, y)
      self.x = x; self.y = y
      self.from = 'Base'
    end
    ------

    -- before set Sub extends Base make sure no any inheritance
    assert.is_false(SubClass:instanceof(BaseClass))
    local sub2 = SubClass:new(nil, 8, 16) -- new instance with call constructor
    assert.same('nil:nil', tostring(sub2.x) .. ':' .. tostring(sub2.y))

    -- set superclass
    SubClass:extends(BaseClass)

    assert.is_true(SubClass:instanceof(BaseClass)) -- SubClass extends Base
    assert.is_true(BaseClass:instanceof(Object))   -- Base extends Object
    assert.is_true(SubClass:instanceof(Object))    -- SubClass -> Base -> Object

    local s3 = SubClass:new(nil, 8, 16)            -- call constructor from Base
    assert.same('Base8:16', tostring(s3.from) .. s3.x .. ':' .. s3.y)
  end)

  --
  it("Object.extends keep fields from superclass", function()
    local BaseClass = class.new_class(Object, 'oop.BaseClass', {
      field = 'default-from-base',
      field2 = 'base'
    })
    assert.same('default-from-base', BaseClass.field)
    assert.same('base', BaseClass.field2)

    local SubClass = class.new_class(BaseClass, 'oop.SubClass', {
      field = 'overriden-by-sub'
      -- field2 not changed
    })
    assert.same('overriden-by-sub', SubClass.field)
    assert.same('default-from-base', BaseClass.field)
    assert.same('base', SubClass.field2)

    local obj = SubClass:new()
    assert.same('overriden-by-sub', obj.field)
    assert.same('base', obj.field2)

    local obj2 = SubClass:new({
      field2 = 'new-for-obj2'
    })
    assert.same('overriden-by-sub', obj2.field)
    assert.same('new-for-obj2', obj2.field2)
    assert.same('base', SubClass.field2)
    assert.same('base', SubClass.field2)

    -- make sure obj2 not affected to its Class
    local obj3 = SubClass:new()
    assert.same('base', obj3.field2)
  end)


  --
  it("call Class", function()
    -- call the "oop.Object"-class itself as a function to instantiate
    -- a new instance is not work. use Object:new() instead.
    -- This is only available to classes that inherit from it
    assert.match_error(function()
      local obj = Object() -- will not work
      assert.is_nil(obj)
    end, "attempt to call upvalue 'Object' .a table value.")

    local obj = Object:new() -- ok

    assert.is_true(base.is_object(obj))
    assert.is_false(base.is_class(obj))

    -- create SubClass extends Object ( analog of class.new_class('SubClass', Object)
    -- "SubClass inherit from Object"
    local SubClass = class.new_class(Object, 'oop.SubClass')

    -- call table of the Class - under the hood fires __call
    -- this mechanic is used at the class definition stage to add fields and methods
    assert.same(SubClass, SubClass({ new_field = true }))
    assert.same(true, SubClass.new_field)

    -- same meaning. syntax sugar:
    local ret = SubClass {
      _init = H.func_self_xy_ret,
      method = function(x) return x end,
    }
    assert.same(SubClass, ret) -- return self
    assert.is_function(SubClass.method)

    -- call function as a public static method
    assert.same(8, SubClass.method(8))

    class.build(SubClass)
    local sub = SubClass(8, 16)
    assert.same('oop.SubClass:8:16', sub.ret)
  end)

  -- "keyword" super available only at _init(constructor)
  -- for call constructor of the parent-class
  it("super", function()
    local SubClass = class.new_class(Object, 'oop.SubClass')
    -- define constructor
    function SubClass:_init(x, y)
      H.func_self_xy_ret(self, x, y) -- update self.ret
    end

    -- method super intended to use in _init constructor only by instance
    assert.is_nil(Object.super)
    assert.is_nil(SubClass.super)

    class.build(SubClass)

    -- added only after the class definition is complete
    assert.same(class._Class.wrapped_super, SubClass.super)
    assert.is_nil(Object.super) -- not available for oop.Object itself

    ----
    local super_in_init = nil
    local SubSubClass = class.new_class(SubClass, 'oop.SubSubClass')

    function SubSubClass:_init(x, y, z)
      super_in_init = self.super
      self:super(x, y) -- call constructor of the parent class
      self.ret = self.ret .. ':' .. tostring(z)
    end

    class.build(SubSubClass)

    local obj = SubSubClass(8, 16, 32)
    assert.same('oop.SubSubClass:8:16:32', obj.ret)
    assert.equal(class._Class.wrapped_super, super_in_init)
  end)

  it("hash", function()
    local SubClass = class.new_class(Object, 'oop.SubClass')
    assert.match('[%a%d]+', '12 3')
    assert.match('[%a%d]+', Object:hash())         -- hex num
    assert.match('[%a%d]+', SubClass:hash())       -- hex num
    assert.match('[%a%d]+', SubClass:new():hash()) -- hex num
    assert.are_not_same(SubClass:new():hash(), SubClass:hash())
    assert.are_not_same(SubClass:hash(), Object:hash())
    assert.same(Object:hash(), Object:hash())
  end)

  it("toArray", function()
    local instance = Object:new({ a = 1, b = { c = 3 } })
    local exp = { b = { c = 3 }, a = 1 }
    assert.same(exp, instance:toArray())
  end)

  it("toArray make sure deep-copy without metatables", function()
    local instance = Object:new({ a = 1, b = Object:new({ c = 3 }) })
    local exp = { b = { c = 3 }, a = 1 }
    local res = instance:toArray()
    assert.same(exp, res)
    assert.is_not_nil(getmetatable(instance.b))
    assert.is_nil(getmetatable(res.b))
    assert.are_not_equal(instance.b, res.b)
    instance.b = false
    assert.same({ c = 3 }, res.b) -- not affected try copy not a ref
  end)


  --                      serialize / desirialize                          --

  it("toArray serialize", function()
    local instance = Object:new({ a = 1, b = Object:new({ c = 3 }) })
    local exp = {
      _class = base.ObjectClassName,
      a = 1,
      b = { _class = base.ObjectClassName, c = 3 }
    }

    local serialized = instance:toArray(nil, true)
    assert.same(exp, serialized)
    assert.is_not_nil(getmetatable(instance.b))
    assert.is_nil(getmetatable(serialized.b))
    assert.are_not_equal(instance.b, serialized.b)
    instance.b = false
    assert.same({ _class = base.ObjectClassName, c = 3 }, serialized.b)
  end)

  it("fromArray desirialize", function()
    local instance = Object:new({
      a = { 8 }, b = Object:new({ c = 32, d = Object:new({ e = { 'E' } }) }, 16)
    })

    local serialized = instance:toArray(nil, true)
    assert.same(base.ObjectClassName, serialized._class)

    -- make sure Object is registered
    assert.is_table(base.class_for(Object._cname))

    -- base.D.enable_module(base)
    local rep_instance = Object.fromArray(nil, serialized) -- workload
    assert.same(instance, rep_instance)

    assert.same(base.ObjectClassName, rep_instance:getClassName())
    assert.same(Object, rep_instance:getClass())
    assert.same(Object, rep_instance.b.d:getClass())
    assert.same({ 'E' }, rep_instance.b.d.e)
  end)

  --
  it("fromArray desirialize fail in object itself", function()
    local instance = Object:new({ a = 8, b = Object:new({ c = 32, d = 4 }) })

    local serialized = instance:toArray(nil, true)
    serialized._class = 'not_existed_classname'

    assert.match_error(function()
      Object.fromArray(nil, serialized, true) -- workload
    end, 'Not Found class "not_existed_classname"')
  end)

  --
  it("fromArray desirialize fail in field of object", function()
    local instance = Object:new({ a = 8, b = Object:new({ c = 32, d = 4 }) })

    local serialized = instance:toArray(nil, true)
    serialized.b._class = 'not_existed_classname'

    assert.match_error(function()
      Object.fromArray(nil, serialized, true) -- workload
    end, 'Not Found class "not_existed_classname" for field "b"')
  end)

  -- case try to use Class, Instance or TableWithMetatable as simple array with
  -- serialized data.
  -- Goal: To avoid possible infinity loop with metatables
  it("fromArray wrong usage", function()
    local serialized_arr = {
      _class = base.ObjectClassName,
      a = 1,
      b = { _class = base.ObjectClassName, c = 3 }
    }
    -- correct usage
    assert.is_not_nil(Object.fromArray(nil, serialized_arr, true)) -- ok

    -- wrong usage Class instead simple table(serialized_arr)
    local MyClass = class.new_class(Object, 'app.MyClass')
    assert.match_error(function()
      Object.fromArray(nil, MyClass, true)
    end, 'arg#2 expected simple table got class: app.MyClass')

    -- wrong usage Instance
    local instance = Object:new({ 1 })

    assert.match_error(function()
      Object.fromArray(nil, instance, true)
    end, 'arg#2 expected simple table got instance of class: oop.Object')

    -- wrong usage TableWithMetatable
    local t_with_mt = {}
    setmetatable(t_with_mt, {})

    assert.match_error(function()
      Object.fromArray(nil, t_with_mt, true)
    end, 'arg#2 expected simple table without metatable')

    assert.is_nil(getmetatable(Object))
    assert.match_error(function()
      Object.fromArray(nil, Object, true)
    end, 'arg#2 expected simple table got class: oop.Object')
  end)
end)

--
-- To save space in the context of one serialization, create a map that stores
-- mappings between the class name and ID.
-- ClassId are unique only within one serialization.
--
describe('oop.Object serialize& desirialize with classname mapping', function()
  before_each(function()
    base._clear_state()
    base.D.disable()
  end)

  it("get_class_id", function()
    local id2cname = new_cname2id()
    assert.same(2, get_class_id(id2cname, 'MyClass'))
    local exp = { MyClass = 2, ['oop.Object'] = 1, next_id = 3 }
    assert.same(exp, id2cname)
    assert.same(2, get_class_id(id2cname, 'MyClass'))

    assert.same(3, get_class_id(id2cname, 'SecondClass'))
    assert.same(3, get_class_id(id2cname, 'SecondClass'))
    local exp_map = {
      ['oop.Object'] = 1,
      MyClass = 2,
      SecondClass = 3,
      next_id = 4
    }
    assert.same(exp_map, id2cname)
  end)

  it("invertMap", function()
    local id2cname = { MyClass = 1, SecondClass = 2, next_id = 3 }
    local exp = { [1] = 'MyClass', [2] = 'SecondClass' }
    assert.same(exp, invert_map(id2cname))
  end)

  it("toArray serialize", function()
    local instance = Object:new({ a = 1, b = Object:new({ c = 3 }) })
    ---@cast instance oop.Object
    local cname2id = {}
    local serialized = instance:toArray(nil, true, cname2id)
    local exp_serialized = { b = { c = 3, _clsid = 1 }, a = 1, _clsid = 1 }
    assert.same(exp_serialized, serialized)

    local exp_map = { next_id = 2, ['oop.Object'] = 1 }
    assert.same(exp_map, cname2id)

    -- workaround to hide lsp warnings
    ---@diagnostic disable-next-line: undefined-field
    local instance_b = rawget(instance, 'b')
    ---@diagnostic disable-next-line: undefined-field
    assert.equal(instance_b, instance.b)

    assert.is_not_nil(getmetatable(instance_b))
    assert.is_nil(getmetatable(serialized.b))
    assert.are_not_equal(instance_b, serialized.b)

    assert.same({ c = 3, _clsid = 1 }, serialized.b)
  end)

  it("fromArray desirialize", function()
    local cname2id = { next_id = 2, ['oop.Object'] = 1 }
    local id2cname = invert_map(cname2id)
    local serialized = {
      _clsid = 1,
      a = 'value-of-a',
      b = {
        _clsid = 1,
        c = 'value-of-b.c',
      }
    }

    -- make sure Object is registered
    assert.is_table(base.class_for(Object._cname))

    local obj = Object.fromArray(nil, serialized, true, id2cname) -- workload
    local exp = { a = 'value-of-a', b = { c = 'value-of-b.c' } }
    assert.same(exp, obj)

    assert.same(base.ObjectClassName, obj:getClassName())
    assert.same(Object, obj:getClass())
    assert.same(Object, obj.b:getClass())
    assert.same('value-of-a', obj.a)
    assert.same('value-of-b.c', obj.b.c)
  end)

  it("fromArray desirialize 2", function()
    local instance = Object:new({
      a = { 8 }, b = Object:new({ c = 32, d = Object:new({ e = { 'E' } }) }, 16)
    })

    local cname2id = {}
    local serialized = instance:toArray(nil, true, cname2id)
    local exp_serialized = {
      _clsid = 1,
      a = { 8 },
      b = {
        _clsid = 1,
        c = 32,
        d = {
          _clsid = 1,
          e = { 'E' }
        },
      },
    }
    assert.same(exp_serialized, serialized)
    assert.same(nil, serialized._class)
    assert.same(1, serialized._clsid)
    assert.same({ next_id = 2, ['oop.Object'] = 1 }, cname2id)

    -- make sure Object is registered
    assert.is_table(base.class_for(Object._cname))

    local id2cname = invert_map(cname2id)
    assert.same({ [1] = 'oop.Object' }, id2cname)
    -- base.D.enable_module(base, true)

    local rep_instance = Object.fromArray(nil, serialized, true, id2cname) -- workload

    assert.same(instance, rep_instance)

    assert.same(base.ObjectClassName, rep_instance:getClassName())
    assert.same(Object, rep_instance:getClass())
    assert.same(Object, rep_instance.b.d:getClass())
    assert.same({ 'E' }, rep_instance.b.d.e)
  end)

  it("fromArray serialize desirialize list", function()
    local o = Object:new({
      list = { Object:new({ id = 1 }), Object:new({ id = 2 }) }
    })

    local cname2id = {}
    local serialized = o:toArray(nil, true, cname2id)
    local exp = {
      _clsid = 1,
      list = { { id = 1, _clsid = 1 }, { id = 2, _clsid = 1 } },
    }
    assert.same(exp, serialized)

    -- desirialize
    local id2cname = invert_map(cname2id)
    local ro = Object.fromArray(nil, serialized, true, id2cname) -- workload

    local exp_ro = { list = { { id = 1 }, { id = 2 } } }
    assert.same(exp_ro, ro)
    assert.same(true, class.is_object(ro.list[1]))
    assert.same(true, class.is_object(ro.list[2]))

    assert.same(o, ro)
  end)

  it("fromArray serialize desirialize Object field", function()
    local ClassA = class.build(class.new_class(Object, 'ClassA'))
    local ClassB = class.build(class.new_class(Object, 'ClassB'))

    local instance = ClassA:new({
      obj = Object:new({ id = 1 }),
      a = ClassA:new({ id = 2 }),
      b = ClassB:new({ id = 3 }),
    })

    local cname2id = {}
    local serialized = instance:toArray(nil, true, cname2id)
    local exp = {
      _clsid = 2,
      b = { _clsid = 3, id = 3 },
      a = { _clsid = 2, id = 2 },
      obj = { _clsid = 1, id = 1 }
    }
    assert.same(exp, serialized)

    -- desirialize
    local id2cname = invert_map(cname2id)
    local ro = Object.fromArray(nil, serialized, true, id2cname) -- workload

    local exp_ro = { obj = { id = 1 }, a = { id = 2 }, b = { id = 3 } }
    assert.same(exp_ro, ro)
    assert.same(true, class.is_object(ro.obj))
    assert.same(true, class.is_object(ro.a))
    assert.same(true, class.is_object(ro.b))

    assert.same(instance, ro)
  end)


  it("toArray dead lock refs", function()
    local obj1 = Object:new({ id = 1 })
    local obj2 = Object:new({ id = 2 })
    obj1.ref_1_to_2 = obj2
    obj2.ref_2_to_1 = obj1
    local res = obj1:toArray()

    local exp_1 = {
      id = 1,
      ref_1_to_2 = nil
    }
    local exp_2 = {
      id = 2,
      ref_2_to_1 = exp_1
    }
    exp_1.ref_1_to_2 = exp_2

    assert.same(exp_1, res)
  end)


  it("toArray dead lock refs serialize(with ClassesNames)", function()
    local obj1 = Object:new({ id = 1 })
    local obj2 = Object:new({ id = 2 })
    obj1.ref_1_to_2 = obj2
    obj2.ref_2_to_1 = obj1

    local res = obj1:toArray({}, true) -- serialize

    local exp_1 = { _class = 'oop.Object', id = 1, ref_1_to_2 = nil }
    local exp_2 = { _class = 'oop.Object', id = 2, ref_2_to_1 = exp_1 }
    exp_1.ref_1_to_2 = exp_2

    assert.same(exp_1, res)
  end)


  it("fromArray dead lock refs serialize(with ClassesNames)", function()
    local obj_1 = { _class = 'oop.Object', id = 1, ref_1_to_2 = nil }
    local obj_2 = { _class = 'oop.Object', id = 2, ref_2_to_1 = obj_1 }
    obj_1.ref_1_to_2 = obj_2

    local object = Object.fromArray(nil, obj_1)

    local exp_1 = {
      id = 1,
      ref_1_to_2 = nil
    }
    local exp_2 = {
      id = 2,
      ref_2_to_1 = exp_1
    }
    exp_1.ref_1_to_2 = exp_2
    assert.same(exp_1, object)
  end)
end)
