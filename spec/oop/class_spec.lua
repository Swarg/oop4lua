-- 17-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

-- expose private function for testing
_G._TEST = true

local base = require("oop.base")
local class = require("oop.class");
local interface = class.interface --require("oop.interface");
local Object = require("oop.Object")

local H = require('oop.helper')
---@diagnostic disable-next-line: unused-local
local inspect = require("inspect")
---@diagnostic disable-next-line: unused-local
local D = base.D


describe("oop.class", function()
  setup(function() end)
  teardown(function() _G._TEST = nil end)

  before_each(function()
    base._clear_state()
  end)

  after_each(function()
    D.disable()
  end)

  -----------------------------------------------------------------------------


  it("get_package_name", function()
    assert.same('oop', class.get_package_name('oop.Object'))
    assert.same('pkg.oop', class.get_package_name('pkg.oop.Object'))
    assert.is_nil(class.get_package_name('Object'))
    assert.is_nil(class.get_package_name(''))
    assert.is_nil(class.get_package_name(nil))
  end)

  --
  -- just a link for convenience
  it("class.Object", function()
    assert.equal(Object, class.Object)
  end)

  --
  -- the class.new_class is a inner method for under the hood using
  --
  it("new_class (raw)", function()
    local Base = class.new_class() -- class Base extends Object

    assert.is_table(Base)
    assert.same(Object, getmetatable(Base)) -- child of Object
    assert.same('oop.Object_Class1', rawget(Base, '_cname'))

    assert.is_nil(rawget(Base, '_init'))
    assert.is_not_nil(Object.new)
    assert.is_not_nil(Base.new)

    local hook

    -- override
    ---@diagnostic disable-next-line: duplicate-set-field
    function Base:_init(x, y)
      self.x = x
      hook = tostring(x) .. ':' .. tostring(y)
    end

    assert.is_not_nil(rawget(Base, '_init'))

    -- new instance
    local instance = Base:new(nil, 8, 16)

    assert.same(8, instance.x)
    assert.is_nil(Base.x) -- the object field does not fall into the class
    -- make sure _init was called
    assert.same('8:16', hook)
  end)

  it("build", function()
    local SubClass = class.new_class(Object, 'SubClass')
    assert.is_function(SubClass.extends)
    assert.is_function(SubClass.implements)
    class.build(SubClass)
    assert.is_nil(SubClass.extends)
    assert.is_nil(SubClass.implements)
  end)

  -- what is the difference and why inherit through a special method when
  -- there is Object:new()
  it("Object:new() vs class.new_class", function()
    local MyClass = class.MyClass { -- BaseClass extends Object
      field = 'value',
      _init = H.func_self_xy_ret    -- self.ret = self.name + x + y
    }
    class.build(MyClass)

    local obj1 = MyClass(8, 4)           -- obj = new BaseClass(8, 4)
    assert.same('value', obj1.field)
    assert.same('MyClass:8:4', obj1.ret) -- from constructor

    local obj2 = MyClass:new(nil, 16, 32)
    assert.same('value', obj2.field)
    assert.same('MyClass:16:32', obj2.ret)

    -- attempt to create new Class via Object:new()
    local My2 = Object:new({
      field = 'my',
      _init = H.func_self_xy_ret,
    })
    assert.is_true(base.is_object(My2))
    assert.is_false(base.is_class(My2))
    -- This is the main reason why I moved away from this syntax
    -- when calling the Object.new function, the _init - constructor is
    -- automatically called.
    -- But this is not what is needed here.
    assert.same('oop.Object:nil:nil', My2.ret)

    assert.match_error(function()
      local my3 = My2(1, 2) -- obj = new My2() -- create new instance fo Class
      assert.same('oop.Object:1:2', my3.ret)
    end, 'attempt to call a table of Instance')
    -- attempt to "seal" a "class" for use - fails
    -- because the system perceives it not as a class but as an object
    assert.match_error(function()
      class.build(My2)
    end, 'class expected')

    -- this behavior is done specifically to keep the syntax more understandable
    -- and predictable:
    -- use the "new" operator - to instantiate objects and not create classes
    -- to create a new Class use: class.new_class('MyClass') or class.MyClass
  end)

  -- How it works under the hood.
  -- how to make MyClass inherite Object via Object:new()
  -- do not use this trick in your code
  -- this is for understanding how it works and why you shouldn't use it
  it("make MyClass via Object:new()", function()
    local MyClass = Object:new({
      field = 'my',
      _init = H.func_self_xy_ret, -- constructor
    })
    -- now MyClass is instence not a Class:
    assert.is_true(base.is_object(MyClass))
    assert.is_false(base.is_class(MyClass))
    -- Also, at the moment of creating an instance, the constructor
    -- must be called and it happens here:
    assert.same('oop.Object:nil:nil', MyClass.ret)
    -- This is the main reason why moved away from this approach (syntax):
    -- when the Object:new function is used to Create a new inherited Class
    -- the constructor(_init) is automatically called in this way.
    -- But this is not what is needed here.
    -- All it need to do is just create a class for further declaration
    -- that's why a separate function is used to create classes

    -- make MyClass a Class:
    MyClass._ctype = base.CLASS
    MyClass._cname = 'MyClass'

    -- now My Class is a "Class":
    assert.is_true(base.is_class(MyClass))
    assert.is_false(base.is_object(MyClass))

    class.build(MyClass) -- remove extends and implements methods
    assert.same(base.CLASS, MyClass._ctype)


    local obj = MyClass(1, 2)                             -- obj = new MyClass(1, 2)
    assert.same('my', obj.field)
    assert.same('oop.Object:nil:nilMyClass:1:2', obj.ret) -- from constructor
    assert.same(MyClass, obj:getClass())
    assert.same('MyClass', obj:getClassName())
    -- Note: There's an interesting effect here
    -- in fact, the ability to run a class as a function was not specified here
    -- i.e. not added __class (Object.__call)
    -- But this functionality works due to the fact that it is described in the
    -- base class Object

    --  Having discovered this fact, I wanted to use it to avoid specifying the
    -- __call field for each inherit class.
    --  But as it turned out, for this to work, the __call field must be in the
    -- metatable of the table itself, which must be called as a function.
    --  And if the same __call has in the metatable of the metatable, calling
    -- the table as a function will not work.
    local MySubClass = MyClass:new()
    assert.has_error(function()
      local sub = MySubClass(3, 4) -- error
      assert.is_nil(sub)
    end)
  end)

  --
  --
  it("new unnamed class", function()
    local MyClass = class.new_class(Object, 'org.app.MyClass')
    -- name of new Class is not defined - generate automaticly
    local ChildClass = class.new_class(MyClass) -- ChildClass inherit MyClass
    assert.same('org.app.MyClass_Class1', ChildClass:getClassName())
    -- idea: show from which superclass was created a new one
  end)


  --
  -- check is a given value a some Class
  it("is_class", function()
    assert.same(true, class.is_class(Object))
    assert.is_false(class.is_class({}))
    assert.is_false(class.is_class({ _cname = 'wrong' }))

    local MyClass = class.new_class(Object, 'MyClass') -- MyClass extends Object
    assert.is_true(class.is_class(MyClass))
    assert.is_false(class.is_class(MyClass:new()))     -- instanceof MyClass
  end)

  -- check is given value is a some instance of some Class
  it("is_object", function()
    assert.same(false, class.is_object(Object))
    assert.same(true, class.is_object(Object:new()))

    local Base = class.new_class(Object, 'Base') -- Object:extends()
    assert.same(false, class.is_object(Base))
    assert.same(true, class.is_object(Base:new()))

    local MyClass = class.new_class(Base, 'MyClass') --Base:extends()
    assert.same(false, class.is_object(MyClass))
    assert.same(true, class.is_object(MyClass:new()))
  end)

  --
  --
  it("new class via class.NewClassName", function()
    -- create a new class via hooks __index
    local MyClass = class.MyClass

    -- make sure Class was successfyly created:
    assert.is_table(MyClass)
    assert.is_true(class.is_class(MyClass))
    assert.same('MyClass', MyClass:getClassName())
    ---@cast MyClass oop.Object

    assert.same(Object, MyClass:getClass()) -- get Parent Class
  end)

  --
  --
  it("class.extends", function()
    local MyClass = class.MyClass -- MyClass extends Object

    local Base = class.Base       -- Base extends Object
    class.extends(MyClass, Base)  -- set MyClass extends Base

    -- make sure now MyClass is inherit Base
    assert.same(Base, MyClass:getClass()) -- get parent Class

    -- define constructor  in the parent class of MyClass (Base)
    local call = ''

    function Base:_init(x, y)
      self.x = x
      call = call .. tostring(x) .. ':' .. tostring(y)
    end

    -- create new instance of MyClass
    local obj = MyClass:new(nil, 8, 16)

    -- make sure constructor from superclass(Base) was called
    assert.same(8, obj.x)
    assert.same('8:16', call)
  end)

  --
  --
  it("new with given table(defenition)", function()
    local MyClass = class.MyClass -- MyClass extends Object
    local def = { field = true }

    local instance = MyClass:new(def, 8, 16)
    assert.same(true, instance.field)
    assert.equal(def, instance) -- instance was created from a given table
  end)

  --
  --
  it("class.extends", function()
    local Base = class.Base
    local MyClass = class.extends('MyClass', Base) -- class('MyClass', Base)
    assert.same(Base, MyClass:getClass())

    local call = ''

    function Base:_init() call = call .. '|Base' end

    ---@diagnostic disable-next-line: duplicate-set-field
    function MyClass:_init() call = call .. '|MyClass' end

    -- create new instance of MyClass
    local obj = MyClass:new()
    assert.is_true(class.is_object(obj))
    assert.is_false(class.is_class(obj))

    -- make sure constructor from superclass(Base) was called
    assert.same('|MyClass', call)

    call = ''
    ---@diagnostic disable-next-line: duplicate-set-field
    function MyClass:_init()
      Base._init(self) --   this.super()  (call constructor of the parent)
      call = call .. '|MyClass'
    end

    MyClass:new()
    assert.same('|Base|MyClass', call)
  end)

  --
  --
  it("implements success", function()
    local Runnable = interface.new(nil, 'Runnable',
      { run = interface.method() }
    )
    local MyClass = class.MyClass
    assert.is_true(class.is_class(MyClass))

    -- override -- implemented method
    function MyClass:run(t)
      return t
    end

    assert.same(MyClass, MyClass:implements(Runnable))
    assert.has_no_error(function()
      class.build(MyClass) -- checks implements
    end)
  end)

  --
  --
  it("implements fail", function()
    local Runnable = class.interface.Runnable {
      run = interface.method('self', Object, 'data'),
    }
    assert.is_true(base.is_interface(Runnable))

    local MyClass = class.MyClass:implements(Runnable)

    assert.match_error(function()
      class.build(MyClass) -- finalize class definition with checks
    end, "NotImplemented: Runnable.run")
  end)

  --
  --
  it("implements default method (copyied to a class)", function()
    local Runnable = {
      _cname = 'Runnable',
      _ctype = base.INTERFACE,
      -- run is a defualt method - will be copoed into MyClass
      run = function(self, t)
        return self:getClassName() .. tostring(t)
      end
    }
    local MyClass = class.MyClass
    assert.is_nil(rawget(MyClass, 'run')) -- run not implemented
    ---@cast MyClass oop.Object
    MyClass:implements(Runnable)
    assert.same(Runnable.run, rawget(MyClass, 'run'))

    local instance = MyClass:new()
    assert.same('MyClass_default_method!', instance:run('_default_method!'))
  end)

  --
  --
  it("implements attempt to call implements on instance not on Class", function()
    local Runnable = { run = class.abstract_method }
    local MyClass = class.MyClass
    local instance = MyClass:new()

    assert.match_error(function()
      ---@cast instance oop.Object
      instance:implements(Runnable)
    end, "this method can only be called for classes and not objects")
  end)

  --
  --
  it("new with given table", function()
    local MyClass = class.MyClass
    assert.is_true(base.is_class(MyClass))
    assert.same(MyClass, MyClass())                 -- cal table -- fires __call
    assert.same(MyClass, MyClass:extends(Object))   -- return self ignore Object
    assert.same(MyClass, MyClass:extends(Object)()) -- call MyClass()

    assert.match_error(function()
      MyClass:implements({})
    end, 'interface expected')
  end)

  --
  --
  it("class.package way to define package name for a new Class", function()
    class.package 'my.app.core'

    local Base = class.Base -- create a new class

    assert.same('my.app.core.Base', Base:getClassName())
    -- TODO find way to auto apply package to one context and do not use in
    -- another. (case: set package in open module, value still in mem and occurs
    -- in an another module
  end)

  --
  --
  it("on_call in action constructor", function()
    local called = false

    class.package 'app'

    local MyClass = class.MyClass {
      _init = function(self, x, y)
        called = true
        self.ret = tostring(x) .. ':' .. tostring(y)
      end,
      method_say = function(self, say)
        return self:getClassName() .. ' say: ' .. say
      end,
    }
    class.build(MyClass) -- definition is complete

    local obj = MyClass:new(nil, 8, 16)

    assert.is_true(called)
    assert.same('8:16', obj.ret)
    assert.same('app.MyClass say: hi', obj:method_say('hi'))
  end)

  --
  --
  it("new BaseClass syntax example", function()
    ---*type my.app.core.BaseClass
    local BaseClass = (function() -- emulate  require 'my.app.core.BaseClass'
      ---- module my.app.core.Base  ----
      class.package 'my.app.core'

      ---*class my.app.core.BaseClass: Object
      ---*field field string
      ---*field method function
      local M_BaseClass = class.BaseClass {
        -- class definition
        field = 'defined in BaseClass',
        method = base.abstract_method,
      }

      return class.build(M_BaseClass)
      ----     end of module        ----
    end)()

    assert.same(true, base.is_class(BaseClass))
    assert.same(false, base.is_object(BaseClass))
    assert.same('my.app.core.BaseClass', BaseClass:getClassName())
    ---@diagnostic disable-next-line: undefined-field
    assert.same('defined in BaseClass', BaseClass.field)
    ---@diagnostic disable-next-line: undefined-field
    assert.same(base.abstract_method, BaseClass.method)
  end)

  --
  --
  it("Class definition with extends and implements - working syntax", function()
    --
    local BaseClass = (function() -- emulate  require 'my.app.core.Base'
      ---- module my.app.core.Base  ----
      class.package 'my.app.core'
      local BaseClass = class.Base {
        field = 'defined in BaseClass',
      }

      return class.build(BaseClass) --
      ----                          ----
    end)()

    --
    class.package 'my.app.api'
    local Listener = interface.Listener {
      run = interface.method('self', string, 'event')
    }

    assert.same('my.app.api.Listener', Object.getClassName(Listener))
    assert.same(true, base.is_interface(Listener))
    assert.same(false, base.is_class(Listener))
    assert.same(false, base.is_object(Listener))
    assert.is_true(base.is_method_signature(Listener.run))
    assert.is_table(Listener.run)

    assert.match_error(function()
      Listener.run()
    end, base.err_abstract_method_call)

    -- application module

    local MyClass = (function()
      -- module
      class.package 'my.app'

      local MyClass = class.MyClass:extends(BaseClass):implements(Listener) {
        _init = function(self, x, y)
          self.ret = self:getClassName() .. '|' .. tostring(x) .. ':' .. tostring(y)
        end,

        run = function(self, x, y)
          return type(self) .. ':' .. tostring(x) .. ':' .. tostring(y)
        end
      }

      return class.build(MyClass)
    end)()

    local obj = MyClass:new(nil, 8, 16) --- same that MyClass()
    assert.same('my.app.MyClass|8:16', obj.ret)
    assert.same('table:16:8', obj:run(16, 8))

    assert.same('defined in BaseClass', obj.field)
  end)

  --
  --
  it("MyClass definition variant 2 success", function()
    local BaseClass = class.Base
    local Runnable = H.mk_interface('Runnable', 'run')

    local MyClass = (function()
      ---- [>>>]  module: my.app.MyClass  [>>>] ----
      class.package 'my.app'

      local MyClass = class.MyClass:extends(BaseClass):implements(Runnable)

      function MyClass:_init(x, y)
        self.ret = self:getClassName() .. '|' .. tostring(x) .. ':' .. tostring(y)
      end

      function MyClass:run(x, y)
        return type(self) .. ':' .. tostring(x) .. ':' .. tostring(y)
      end

      return class.build(MyClass)
      ---- [END]  module: my.app.MyClass  [<<<] ----
    end)()

    local obj = MyClass:new(nil, 8, 16) --- same that MyClass()
    assert.same('my.app.MyClass|8:16', obj.ret)
    assert.same('table:16:8', obj:run(16, 8))
  end)

  --
  --
  it("MyClass definition variant 2 - fail: not implemented", function()
    local BaseClass = class.Base
    local Runnable = H.mk_interface('Runnable', 'run')

    local requireMyClass = function()
      class.package 'my.app'

      local MyClass = class.MyClass:extends(BaseClass):implements(Runnable)

      function MyClass:_init(x, y)
        self.ret = self:getClassName() .. '|' .. tostring(x) .. ':' .. tostring(y)
      end

      -- "run" is not implemented!

      return class.build(MyClass)
    end

    local MyClass

    assert.match_error(function()
      requireMyClass() -- local MyClass  = require('my.app.MyClass')
    end, 'NotImplemented: Runnable.run')

    assert.is_nil(MyClass)
  end)

  --
  --
  it("working syntax define MyClass and create instance via call tbl", function()
    local BaseClass = class.Base
    local Runnable = H.mk_interface('Runnable', 'run')
    local MyClass = class.MyClass:extends(BaseClass):implements(Runnable) {
      field = true,
      _init = function(self, x, y) self.ret = tostring(x) .. tostring(y) end,
      run = function() end,
    }

    class.build(MyClass)
    local obj = MyClass('[', ']') -- obj = new MyClass(..)
    assert.same(true, obj.field)
    assert.same('[]', obj.ret)
  end)

  --
  --
  it("class.interface.MyInterface under the hood", function()
    local MyInterface = class.interface.MyInterface

    assert.same(true, base.is_interface(MyInterface))
    assert.same(false, base.is_class(MyInterface))
    assert.same(false, base.is_object(MyInterface))

    -- make sure class not broken and can create new Classes
    local MyClass = class.MyClass
    assert.same(false, base.is_interface(MyClass))
    assert.same(true, base.is_class(MyClass))
    assert.same(false, base.is_object(MyClass))
  end)

  --
  --
  it("class.interface.Listener", function()
    local Listener = class.interface.Listener {
      handle = base.abstract_method
    }
    assert.same(true, base.is_interface(Listener))
    assert.same(false, base.is_class(Listener))
    assert.same(base.abstract_method, Listener.handle)

    assert.has_error(function() Listener.handle() end, base.err_abstract_method_call)
  end)

  --
  --
  it("class.interface.EventListener extends Listener", function()
    local Listener = interface.Listener {
      handle = interface.method('self')
    }
    local EventListener = interface.EventListener:extends(Listener) {
      post_event = interface.method('self') --base.abstract_method
    }
    assert.same(true, base.is_interface(EventListener))

    assert.is_true(base.is_method_signature(EventListener.handle))
    assert.is_true(base.is_method_signature(EventListener.post_event))

    assert.is_true(base.is_method_signature(Listener.handle))
    assert.is_nil(Listener.post_event)
  end)

  --
  --
  it("class.interface.EventListener extends Listener", function()
    local Listener = class.interface.Listener {
      handle = base.abstract_method
    }
    local EventListener = class.interface.EventListener:extends(Listener) {
      post_event = base.abstract_method
    }

    class.package 'my.app.core'

    local EventBus = class.EventBus:implements(EventListener) {
      _init = function(self, state) -- constructor
        -- print('instance:' .. self)
        self.cnt = 0
        self.state = state
      end,
      handle = function(self, event)
        self.cnt = self.cnt + 1
        return 'handle:' .. tostring(event)
      end,
      post_event = function(self, event)
        self.state[#self.state + 1] = event
        return 'post_event:' .. tostring(event)
      end,
    }
    assert.same(base.CLASS, EventBus._ctype)
    assert.is_function(EventBus.extends)
    EventBus = class.build(EventBus)
    assert.is_nil(EventBus.extends)

    local state = {}
    local state2 = {}
    -- create new instance of the EventBus -- same as EventBus:new(nil, state)
    local bus = EventBus(state) -- new EventBus(state)
    local bus2 = EventBus:new(nil, state2)

    assert.same(bus, bus2)

    assert.same(false, base.is_interface(bus))
    assert.same(false, base.is_class(bus))
    assert.same(true, base.is_object(bus))

    assert.same(state, bus.state)
    assert.same({}, bus.state)
    assert.same('handle:x', bus:handle('x'))
    assert.same('post_event:y', bus:post_event('y'))
    assert.same({ 'y' }, bus.state)

    assert.same(true, bus:instanceof(EventBus))
    -- print("[DEBUG] EventBus:", EventBus)
    -- print("[DEBUG] bus:", bus, inspect(bus))
    -- print("[DEBUG] bus2:", bus2, inspect(bus2))
    assert.are_not_same(bus, bus2)
  end)

  --
  --
  it("super", function()
    -------
    class.package 'oop'

    local SubClass = class.A {
      _init = function(self, x, y)     -- define constructor
        self.super()                   -- call Object._init

        H.func_self_xy_ret(self, x, y) -- update self.ret
      end,
    }
    class.build(SubClass) -- complete the class definition
    --------

    class.package 'oop'

    local SubSubClass = class.SubSubClass:extends(SubClass) {

      _init = function(self, x, y, z)
        self:super(x, y) -- call the constructor of the superclass (SubClass._init)

        self.ret = self.ret .. ':' .. tostring(z)
      end
    }
    class.build(SubSubClass)

    local obj = SubSubClass(8, 16, 32) -- new SubSubClass(...)  call _init
    assert.same('oop.SubSubClass:8:16:32', obj.ret)

    assert.is_not_nil(obj.super)
    assert.is_not_nil(SubSubClass.super)
  end)

  --
  --
  it("super inherited with gaps", function()
    local A = class.A {
      _init = function(self, x, y)     -- define constructor
        self:super()                   -- call Object._init
        H.func_self_xy_ret(self, x, y) -- update self.ret
      end,
    }
    local B = class.new_class(A, 'B', {})
    local C = class.new_class(B, 'C', {})
    local D1 = class.new_class(C, 'D1', {
      _init = function(self, x, y, z)
        self:super(x, y) -- call A._init
        H.func_self_xy_ret(self, y, z)
      end,
    })
    H.build_classes(A, B, C, D1)

    local obj = D1(8, 16, 32)

    assert.same('D1:8:16D1:16:32', obj.ret)
  end)

  --
  --
  --
  it("super inherited A - E", function()
    local A = class.A {
      _init = function(self, x, y)
        H.func_self_xy_ret(self, x, y)
      end,
    }
    local B = class.new_class(A, 'B', {})
    local C = class.new_class(B, 'C', {})
    local D1 = class.new_class(C, 'D1', {
      _init = function(self, x, y, z)
        self:super(x, y) -- call A._init
        self.ret = tostring(self.ret) .. ':' .. tostring(z)
      end,
    })
    local E = class.new_class(D1, 'E', {})

    H.build_classes(A, B, C, D1, E)

    local obj = E(8, 16, 32)

    -- make sure _init from class A was called via _init in class D1
    assert.same('E:8:16:32', obj.ret)
  end)


  it("super inherited A - E", function()
    local A = class.A {
      _init = function(self, x)
        self.s = 'A' .. tostring(x)
      end,
    }
    local B = class.new_class(A, 'B', {
      _init = function(self, x, y)
        self:super(x)
        self.s = self.s .. ':B' .. tostring(y)
      end,
    })
    local C = class.new_class(B, 'C', {
      _init = function(self, x, y, z)
        self:super(x, y)
        self.s = self.s .. ':C' .. tostring(z)
      end,
    })
    local D1 = class.new_class(C, 'D', {
      _init = function(self, x, y, z, k)
        self:super(x, y, z)
        self.s = self.s .. ':D' .. tostring(k)
      end,
    })
    local E = class.new_class(D1, 'E', {
      _init = function(self, x, y, z, k, l)
        self:super(x, y, z, k)
        self.s = self.s .. ':E' .. tostring(l)
      end,
    })

    H.build_classes(A, B, C, D1, E)

    local obj = E(2, 4, 8, 16, 32)

    -- make sure all super works corretly
    -- chain of calls to superclass constructors
    assert.same('A2:B4:C8:D16:E32', obj.s)
  end)

  --
  --
  --
  it("toArray inherited", function()
    local SuperClass = class.MyClass {
      -- public static fields:
      field_a = 8,
      field_b = 16,

      -- constructor
      _init = function(self, c)
        self.f_c = c -- dynamic field (created in object)
      end
    }
    class.build(SuperClass)

    local SubClass = class.A:extends(SuperClass) {
      -- public static fields:
      f_d = 64,
      f_e = 128,
      -- constructor
      _init = function(self, c, f)
        self:super(c) -- call constructor of superclass
        self.f_f = f
      end
    }
    class.build(SubClass)

    local obj = SubClass(32, 256)
    local exp = {
      field_a = 8, field_b = 16, f_c = 32, f_d = 64, f_e = 128, f_f = 256,
    }
    assert.same(exp, obj:toArray())
  end)

  --
  --
  it("check_class_definition fail", function()
    local Event = class.Event

    local Listener = interface.Listener {
      run = interface.method('self'),
      handle = interface.method('self', Event, 'event'),
    }
    assert.is_true(base.is_method_signature(Listener.run))
    assert.is_table(Listener.run)

    local C = class.C ---@type oop.Object
    C:implements(Listener)

    -- make sure that abstract methods from interface do not copied into class
    assert.same({}, C:toArray())
    ---@diagnostic disable-next-line: undefined-field
    assert.is_nil(C.handle)
    assert.is_table(C._implements) -- list of all implemented interfaces
    assert.match_error(function()
        class.check_class_definition(C)
      end, "NotImplemented: Listener.handle\n" ..
      "NotImplemented: Listener.run")

    assert.is_false(C:instanceof(Listener))
    assert.is_false(C._implements[Listener])
  end)

  --
  it("check_class_definition fail 2", function()
    local Listener = interface.Listener {
      run = interface.method('self'),
    }
    local C = class.C:implements(Listener)

    assert.match_error(function()
      class.build(C)
    end, "NotImplemented: Listener.run")
    assert.is_false(C:instanceof(Listener))
  end)

  --
  --
  it("check_class_definition", function()
    local Listener = interface.Listener {
      run = interface.method('self'),
    }
    local C = class.C:implements(Listener) {
      run = function() end, -- implemented
    }
    ---@cast C oop.Object
    class.build(C)  -- ok
    assert.is_true(C:instanceof(Listener))
    local obj = C() -- new C()
    assert.is_true(obj:instanceof(Listener))
  end)


  --
  --
  it("inherit chain", function()
    local A = class.A {
      methodA = function(_, x) -- _ is self
        return 'A:' .. tostring(x)
      end
    }
    assert.same('A:8', A.methodA(nil, 8))
    assert.same('A:8', A:new():methodA(8))

    local B = class.new_class(A, 'B', {
      methodB = function(_, x)
        return 'B:' .. tostring(x)
      end
    })
    assert.same('A:8', B:new():methodA(8))
    assert.same('B:8', B:new():methodB(8))

    local C = class.new_class(B, 'C', {
      methodC = function(_, x)
        return 'C:' .. tostring(x)
      end
    })
    assert.same('A:8', C:new():methodA(8))
    assert.same('B:8', C:new():methodB(8))
    assert.same('C:8', C:new():methodC(8))
    -- print("[DEBUG] C:", require'inspect'(C))
  end)

  --
  it("name", function()
    assert.same('oop.Object', class.name(Object))
    assert.same('MyClass', class.name(class.new_class(nil, 'MyClass', nil)))
    assert.same('AClass', class.name(class.new_class(nil, 'AClass'):new()))
    assert.same('IListener', class.name(interface.new(nil, 'IListener')))
    assert.is_nil(class.name(''))
    assert.is_nil(class.name('x'))
    assert.same('name', class.name({ _cname = 'name' }))
  end)

  --
  it("try to inherit via new instance", function()
    class.package 'io'
    local Builder = class.new_class(Object, 'Builder', {})
    local SubBuilder = Builder:new({
      field = 13
    })

    local errmsg = "build:class expected has: " ..
        "object : class(io.Builder) : class(oop.Object)"

    assert.match_error(function()
      class.build(SubBuilder) -- attempt to build instance as class
    end, errmsg, 1, true)
  end)
end)
