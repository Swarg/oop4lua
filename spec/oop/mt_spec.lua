require("busted.runner")()
local assert = require("luassert")

-- Experimental. Research how a metatables works

--
describe("oop.class", function()
  it("equal", function()
    local t1 = {}
    local t2 = {}
    local t3 = t1
    assert.same(t1, t1)
    assert.same(t1, t2)
    assert.are_not_equal(t1, t2)
    assert.is_false(t1 == t2)
    assert.is_true(t1 == t3)
    assert.equal(t1, t3)
  end)

  --
  --
  it("rawget", function()
    local t = { id = 1 }
    assert.same(1, rawget(t, 'id'))

    local t2 = {}
    local meta = { -- table with Class
      id = 2
    }
    meta.__index = meta
    setmetatable(t2, meta)

    assert.same(2, t2.id) -- take from ... t2.metatable.__index ... id
    assert.is_nil(rawget(t2, 'id'))
  end)

  it("setmetatable", function()
    local t, meta = {}, {}
    assert.is_true(t == setmetatable(t, meta)) -- ret is t
  end)

  --
  --
  it("setmetatable", function()
    local called = false
    local t = {}
    local meta = {
      __call = function() -- (tbl, ...)
        called = true
        return 'works'
      end
    }
    setmetatable(t, meta)
    assert.same('works', t())
    assert.is_true(called)
  end)

  --
  --
  it("setmetatable", function()
    local called = false
    local t = {}
    local meta = {
      __call = function() -- (tbl, ...)
        called = true
        return 'works'
      end
    }
    meta.__index = meta
    setmetatable(t, meta)
    assert.same('works', t())
    assert.is_true(called)

    local sub = setmetatable({}, t)
    t.__index = t
    assert.has_error(function()
      sub()
    end)
    -- fix:
    t.__call = meta.__call
    assert.same('works', sub())
  end)

  --
  --
  it("__index access to field fail no metatable", function()
    local t = {
      __index = function() return 8 end
    }
    assert.no_error(function()
      assert.is_nil(t.x) -- attempt to call local 't' (a table value)
    end)
  end)

  -- conclusion: for the __index to work must exists metatable
  it("__index access to field success ", function()
    local mt = { __index = function() return 8 end }
    local t = setmetatable({}, mt)
    assert.same(8, t.x) -- ok
  end)

  it("__index access to field fail", function()
    local t = { __index = function() return 8 end }
    t = setmetatable(t, {})
    assert.is_nil(t.x)
  end)

  --
  --
  it("__tostring", function()
    local o = {}
    o.__index = o
    local t = setmetatable(o, {
      __tostring = function()
        return 'works (msg from-mt)'
      end
    })
    assert.equal(o, t)
    assert.same('works (msg from-mt)', tostring(t)) -- fires __tostring from metatable
    assert.is_nil(rawget(t, '__tostring'))
    assert.is_function(rawget(getmetatable(t), '__tostring'))

    -- a new one
    t.__tostring = function() return 'from self' end
    assert.is_function(rawget(t, '__tostring'))
    -- make sure t.__tostring was ignored
    assert.same('works (msg from-mt)', tostring(t)) -- use t2.mt.__tostring
  end)

  it("__tostring", function()
    local t = { _cname = 1 }
    assert.same(1, t._cname)
    assert.same(1, rawget(t, '_cname'))
    assert.is_nil(rawget(t, 'x'))
    -- assert.has_error(function() rawget(nil, 'x') end) --
  end)

  --
  -- used to check resining on how to get object hash
  it("__tostring extends", function()
    local t = setmetatable({}, {
      __tostring = function()
        return 'works (msg from-mt)'
      end
    })
    t.__index = t
    --
    local t2 = setmetatable({}, t)
    assert.match('table: 0x[%d]+', tostring(t2)) -- broken
    assert.is_nil(rawget(t2, '__tostring'))

    t.__tostring = getmetatable(t).__tostring
    assert.same('works (msg from-mt)', tostring(t2))
  end)

  it("spy", function()
    local called = false
    local say = function(_, x)
      called = true
      return x
    end

    local obj = { say = say }
    -- assert.same(8, obj.say(8))

    local s = spy.on(obj, "say")
    -- print("[DEBUG] s:", require('inspect')(s))
    assert.same(8, obj:say(8))
    assert.same(true, called)

    assert.spy(s).was.called(1)

    assert.spy(s).was.called(1)
    assert.spy(s).was.called_with(obj, 8)
  end)
end)
