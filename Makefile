install:
	sudo luarocks make

VERSION := 0.6.0-1

upload:
	luarocks upload rockspecs/oop-$(VERSION).rockspec

test:
	busted ./spec/
