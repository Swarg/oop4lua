## Overview

Lua Library for object-oriented programming.

Created with a Goal: to provide a simple and clear tool for convenient
development of readable object-oriented code based on classes and objects.
Without having to repeat pieces of code and think about the specific features
of OOP implementation in Lua.

Separate syntax for creating an instances, classes, interfaces, and
method signatures for define interfaces and abstract classes.
Single inheritance, but with a "defualt methods" in interfaces.
The intuitive syntax for class definition with "extends" and "implements"
Automatically checks whether a class actually implements all declared interfaces
Automatically called constructors on creating the instance of class
with simple and clear overriding in inherited classes, provided the self.super
to call constructor of superclass. And support for chaining calls to superclass
constructors via self.super()

Key Features:

- All classes inherit the base class Object.
- Single inheritance, but with a "default methods" in interfaces.
- The intuitive syntax for class definition with "extends" and "implements"
- Auto checking whether the class really implements all the interfaces it declares
- Separate syntax for creating an classes, instances, interfaces and method
  signatures.
- Method signatures can be used to define interface and abstract methods.
- Automatically called constructor on creating the instance of class
  with simple and clear overriding in inherited classes.


## Usage Example:

[See examples/myapp](./examples/myapp/)


[examples/myapp/BaseClass](./examples/myapp/BaseClass.lua)
```lua
---- module my.app.core.Base  ----
local class = require 'oop.class'

class.package 'my.app.core'

---@class my.app.core.BaseClass: oop.Object
local BaseClass = class.BaseClass {
  field = 'static field with defined value',

  -- constructor
  _init = function(self, x, y)
    self.x = x
    self.y = y
  end,

  -- dynamic method
  echo = function(self, input)
    return input
  end,

  ---@return my.app.core.BaseClass
  anotherMethod = function(self, a, b)
    self.a = a
    self.b = b
    return self
  end,
}

-- complete class definition and return a new created Class
return class.build(BaseClass)
```


module with interface
```lua
local class = require 'oop.class'
local interface = class.interface

class.package 'my.app.api'

---@class my.app.api.Listener: oop.Interface
local Listener = interface.Listener {

  ---@param event table
  handle = interface.method('self', class.Object, 'event')

}

return interface.build(Listener)
```


module with child class inherit the parent(superclass)
```lua
-- module my.app.MyClass
local class = require 'oop.class'

class.package 'my.app'

local BaseClass = require 'my.app.core.BaseClass' -- aka import
local Listener = require 'my.app.api.Listener'

---@class my.app.MyClass: my.app.core.BaseClass
local MyClass = class.MyClass :extends(BaseClass) :implements(Listener) {

  _init = function(self, x, y, z)
    self:super(x,y)
    self.z = z
  end,

  ---@param event table
  handle = function(self, event)
    -- do something ...
  end,

  __tostring = function(self)
    return self.x .. ':' .. self.y .. ':' .. self.z
  end,
}

return class.build(MyClass)
```


Now let's put it all together:
[See examples/myapp/run.lua](./examples/myapp/run.lua)
```lua
local MyClass = require 'myapp.MyClass'

-- obj = new Class(8, 16, 32)
local obj = MyClass(8, 16, 32) --- same that MyClass:new(nil, 8, 16, 32)


print('toString     : ', obj)                   -- 8:16:32
print('Handler      : ', obj:handle('event'))   -- 8:16:32 event
print('defaultMehtod: ', obj:defaultMethod(64)) -- myapp.MyClass.defaultMethod 64
print()

```

[Iterator App Example](./examples/iterator/app.lua)


## Documentation


- `oop.class` - factory to provide new Classes
- `oop.interface` - factory to provide new Interfaces
- `oop.Object` - superclass for all Classes
- `oop.base` - inner functional
- `oop.MethodSignature` - structure for defining methods in interfaces and abstract classes



