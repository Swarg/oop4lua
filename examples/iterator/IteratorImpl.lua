-- 09-08-2024 @author Swarg

local class = require 'oop.class'

local Iterator = require 'iterator.Iterator'

class.package 'iterator'
---@class iterator.IteratorImpl: iterator.Iterator
---@field new fun(self, o:table?, list:table, lnum:number?): iterator.IteratorImpl
---@field o table
local Iter = class.IteratorImpl:implements(Iterator) {
  -- constructor
  ---@param lines table
  ---@param offset number?
  _init = function(self, lines, offset)
    assert(type(lines) == 'table', 'lines')

    self.o = { -- state
      lines = lines,
      n = offset or 0,
    }
  end,

  ---@return boolean
  hasNext = function(self)
    return self.o.n < #self.o.lines
  end,

  next = function(self)
    local o = self.o
    o.n = o.n + 1
    return o.lines[o.n]
  end,
}

class.build(Iter) -- check interface impl
return Iter

