--
local class = require 'oop.class'
local interface = class.interface -- or require 'oop.interface'
local method = interface.method

class.package 'iterator'
---@class iterator.Iterator : oop.Object
---@field new fun(self, o:table): iterator.Iterator
---@field hasNext fun(self): boolean
---@field next fun(self): any
local I = interface.Iterator {

  hasNext = method('self', ':', 'boolean'),

  next = method('self', ':', 'any'),
}

interface.build(I)
return I
