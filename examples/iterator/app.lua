-- 09-08-2024 @author Swarg
--
-- an example of how you can use interfaces and classes that
-- implement them using an iterator as an example
--

local IteratorImpl = require 'iterator.IteratorImpl'

local fruits = {
  'Ackee', 'Apple', 'Apricot', 'Banana', 'Blueberry', 'Boysenberry',
  'Currant', 'Date', 'Durian', 'Fig', 'Gooseberry', 'Kiwi', 'Mango',
  'Olive', 'Orange', 'Papaya', 'Pineapple', 'Strawberry', 'Tangerine',
  'Watermelon'
}

-- Iterarot iter = new IteratorImpl(lines)
local iter = IteratorImpl(fruits) --     same that IteratorImpl:new(nil, lines)

local s = ''
while (iter:hasNext()) do
  s = s .. ' ' .. iter:next()
end

print('Fruits:', s)
