local class = require 'oop.class'
local interface = class.interface -- or require 'oop.interface'

class.package 'myapp'

---@class my.app.api.Listener: oop.Interface
---@field handle function(self, event:oop.Object)
---@field defaultMethod function(self, event:oop.Object)
local Listener = interface.Listener {

  -- define signature of method "handle"
  handle = interface.method('self', class.Object, 'event'),

  defaultMethod = function(self, param)
    return tostring(self:getClassName()) .. '.' .. 'defaultMethod' ..
        ' ' .. tostring(param)
  end,
}

return interface.build(Listener)
