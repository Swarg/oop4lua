-- module my.app.MyClass
local class = require 'oop.class'

local BaseClass = require 'myapp.BaseClass' -- import
local Listener = require 'myapp.Listener'

-- Note: put a package definition after import another classes
-- because require module with another classes breaks assigned value
class.package 'myapp'
---@class my.app.MyClass: my.app.core.BaseClass
---@field defaultMethod function (self, param:any)
local MyClass = class.MyClass :extends(BaseClass) :implements(Listener) {

  --
  _init = function(self, x, y, z)
    print('[TRACE] MyClass constructor(_init)', x, y, z)
    self:super(x, y) -- call constructor of BaseClass
    self.z = z
  end,

  ---@param event table
  ---@diagnostic disable-next-line: unused-local
  handle = function(self, event)
    return tostring(self) .. ' ' .. tostring(event)
  end,

  --
  __tostring = function(self)
    return self.x .. ':' .. self.y .. ':' .. self.z
  end,
}


return class.build(MyClass)
