-- local class = require 'oop.class'
-- local base = require 'oop.base'


local MyClass = require 'myapp.MyClass'

-- obj = new Class(8, 16, 32)
local obj = MyClass(8, 16, 32) --- same that MyClass:new(nil, 8, 16, 32)


print('toString     : ', obj)                   -- 8:16:32
print('Handler      : ', obj:handle('event'))   -- 8:16:32 event
print('defaultMehtod: ', obj:defaultMethod(64)) -- myapp.MyClass.defaultMethod 64
print()

-- [TRACE] MyClass constructor(_init)	8	16	32
-- [TRACE] BaseClass constructor(_init)	8	16
-- toString     : 	8:16:32
-- Handler      : 	8:16:32 event
-- defaultMehtod: 	myapp.MyClass.defaultMethod 64


local Listener = require 'oop.class'.forName('myapp.Listener') -- load registered
---@cast Listener my.app.api.Listener
print('defaultMehtod: ', Listener:defaultMethod(24))
print('defaultMehtod: ', Listener.defaultMethod(obj, 16))

-- myapp.Listener.defaultMethod 24
-- myapp.MyClass.defaultMethod 16


print()
print('obj instanceof MyClass : ', obj:instanceof(MyClass))
print('obj instanceof Listener: ', obj:instanceof(Listener))
