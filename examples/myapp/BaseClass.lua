local class = require 'oop.class'

class.package 'myapp'

---@class my.app.core.BaseClass: oop.Object
local BaseClass = class.BaseClass {
  field = 'static field with defined value',

  -- constructor
  _init = function(self, x, y)
    print('[TRACE] BaseClass constructor(_init)', x, y)
    self.x = x
    self.y = y
  end,

  -- dynamic method
  ---@diagnostic disable-next-line: unused-local
  echo = function(self, input)
    return input
  end

}

-- another way to define dynamic method of the defined class:

---@return my.app.core.BaseClass
function BaseClass:anotherMethod(a, b)
  self.a = a
  self.b = b
  return self
end

return class.build(BaseClass)
