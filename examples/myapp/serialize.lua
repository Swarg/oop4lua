-- 07-05-2024 @author Swarg
--
-- an example of how to use different types of serialization
--
-- how to run:
--
--   cd examples
--   lua myapp/serialize.lua
--

local class = require 'oop.class'

local Object = class.Object
local MyClass = require 'myapp.MyClass'
local inspect = require 'inspect'



-- new instance of own class
local obj = MyClass(8, 16, 32) --- same that MyClass:new(nil, 8, 16, 32)

print('toSting of Original Instance: ', obj)



print("\n## 1: simple to array (usefull for debugging)")

local array = obj:toArray()
print('as Array: ', inspect(array))

print('Build instance from array...')
local robj = Object.fromArray(MyClass, array)

print('toSting of Replica from Array:', robj)


--
--
--
print("\n## 2: with _class as plain text")

-- simple serialize and deserialize
local serialized = obj:toArray({}, true)
print('Serialized: ', inspect(serialized))

print('Deserializing...')
local dobj = Object.fromArray(nil, serialized)

print('toSting of Original Instance: ', dobj)


--
--
--
print("\n## 3: with class_map")

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id
local invert_map = SH.invert_map

-- serialize and deserialize with classmap
local cname2id = new_cname2id()
local serialized_2 = obj:toArray({}, true, cname2id)
print('ClassMap:', inspect(cname2id))
print('Serialized: ', inspect(serialized_2))

print('Deserializing...')
local id2cname = invert_map(cname2id)
local obj_2 = Object.fromArray(nil, serialized_2, true, id2cname)

print('toSting of Original Instance: ', obj_2)
