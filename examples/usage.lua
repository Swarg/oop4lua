-- 18-11-2023 @author Swarg
local class = require("oop.class")

class.package 'my.app.entities'

local BaseClass = class.BaseClass {
  id = 0,
}

--[[
-- same class definition without syntax sugar:
local BaseClass = class.new_class(class.Object, 'BaseClass', {
  id = 0,
})
]]

return class.build(BaseClass)

