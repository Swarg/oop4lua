## TODO list:

- ReadOnly for defined(completed) classes (after class.build())
- Abstract Class without ability to create new instance from it
- Documentation
- issue: class.package 'pkg'; require 'ClassB'; local classA = class.new_class...
  value of package overrided by class.build from another modules

-- issue: reload code on runtime:
   - old versions of classes hold by instances in its metatable
   - base.classes holds new versions of clases
   - how to sync this? via instanceof? or cast?

```lua
--
--
-- MyClass:instanceof(Base)
--
-- TODO: more safe
-- class.is(obj):instanceof(BaseClass)              -- obj instanceof BaseClass
--                             == Object.instanceof(obj, BaseClass)

-- class.is(obj):are_class()   == class.is_class(obj)
-- class.is(obj):are_object()  == class.is_object(obj)
--
-- function Object:instanceof(class) -- Object.instanceof(instance, class)
```


